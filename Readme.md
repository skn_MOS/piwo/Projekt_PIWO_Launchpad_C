# Projekt P.I.W.O. Launchpad NOVATION MK2

## Installation

It is strongly advied to use prebuilt binaries available in the pipeline.
If one however wants to build project manually follow instructions provided in the system specific installation section below.

### Prebuilt binaries (recommended)

Just download them from the pipeline.

### Manual installation

#### Dependencies

Before building binaries manually one must first provide necessary dependecies.
Check CMakeLists.txt for version requirements.
List of required componets are listed below. To install then just follow their installation steps.

1. mipc - MOS Interprocess Communication Library (https://gitlab.com/skn_mos/libraries/mipc)
2. libpiwo - PIWO format parsing library (https://gitlab.com/skn_mos/libraries/libpiwo)
3. libfmt - A modern formatting library (https://github.com/fmtlib/fmt)

Windows additionally may need:
1. winmm (system library, probably already installed)

Linux additionally may need:
1. libusb - A cross-platform library to access USB devices (https://github.com/libusb/libusb)
2. libpthread - POSIX Thread Library (https://www.gnu.org/software/hurd/libpthread.html)

#### Windows

1. Install __MinGW g++ compiler__ (must support C++20)
2. Install __GNU make__
3. Install __CMake__
4. Add __MinGW__'s bin folder to `PATH` variable (eg. C:\MinGW\bin)
5. Add __GNU make__'s path to `PATH` variable (eg. C:\Program Files\GnuWin32\bin)
6. Add __CMake__'s path to `PATH` variable (eg. C:\Program Files\CMake\bin)

#### Linux

1. Install __g++__ (version 10 or newer is preferred, also must support C++20)
2. Install __make__
3. Install __CMake__

### Building

#### Windows

1. Open terminal (eg. cmd.exe)
2. Go to location of main directory of this repo
3. Create new directory called __build__ with `mkdir build`
4. Enter new directory with `cd build`
5. Type in `cmake -G "Unix Makefiles" ..`
6. Type in `make`
7. Run `lp.exe`
8. Read console output for further steps (if any)

#### Linux

1. Open terminal
2. Go to location of main directory of this repo
3. Create new directory called __build__ with `mkdir build`
4. Enter new directory with `cd build`
5. Type in `cmake ..`
6. Type in `make`
4. Run `./lp`
5. Read console output for further steps (if any)

###### __Mocking file descriptors__ (Linux only)
One can specify different device (file) from which software should read or write instead of
mk2 novation launchpad device.
To do so specify variable for cmake  
CONFIG_MOCK_MK2_READ=`<device path>`  
CONFIG_MOCK_MK2_WRITE=`<device path>`  
for example: cmake -DCONFIG_MOCK_MK2_READ=/dev/fakemk2 -DCONFIG_MOCK_MK2_WRITE=/dev/null ..

###### __Optional:__ (binding animations)
1. Install __Python2__ (optional -- animations binding)
2. Add __Python2__'s path to `PATH` variable (eg. C:\Program Files\Python27)
3. Run bind.py *piwo7 filenames* (or \*.piwo7 to select all piwo7 files from current directory) 

### Usage

You can find user's manuals in docs folder

### Troubleshooting

#### Hardware doesn't respond
##### Windows
Make sure You have installed Novation's official drivers for MK2 launchapd.

##### Linux
1. Make sure You have run mk2register.py at least once and it did not report any errors.
2. Load necessary kernel drivers for MK2 launchpad. You can load generic usbmidi drivers by running load_modules.sh with root privilages,
or You can build custom driver from `mk2/` directory __(NOT RECOMMENDED)__ and load It if You don't know how to get generic drivers to work.

### Contact
If you encounter any problems with any of installation, building or running stage which is not described in this document you
can contact author of this software by email: patryk.wlazlyn@gmail.com or irc: mgot@freenode.

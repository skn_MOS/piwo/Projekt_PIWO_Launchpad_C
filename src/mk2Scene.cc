#include "mk2Scene.h"
#include "launchpad.h"

mk2Scene::mk2Scene(uint32_t memGridWidth, uint32_t memGridHeight, Mk2* p)
  : parent(p)
  , mainTable(memGridWidth, memGridHeight)
{}

void
mk2Scene::flushMainTableToLaunchpad() const
{
	this->parent->flushFrameToLaunchpad(this->mainTable);
}

mk2Scene::~mk2Scene() {}

void
mk2Scene::clearPad()
{
	this->parent->clearPad();
}

int
mk2Scene::reallocMainMem(uint32_t width, uint32_t height)
{
	return this->mainTable.resizeMemory(width, height);
}

void
mk2Scene::restoreGrid()
{
	this->flushMainTableToLaunchpad();
}

void
mk2Scene::restorePositionState()
{
	// uparrow
	if (this->mainTable.currentPosition.y + 8 ==
	    this->mainTable.getHeight())
		this->parent->setLedXY(3, 9, 0x003f0000);
	else
		this->parent->setLedXY(3, 9, 0x00003f00);

	// downarrow
	if (this->mainTable.currentPosition.y == 0)
		this->parent->setLedXY(4, 9, 0x003f0000);
	else
		this->parent->setLedXY(4, 9, 0x00003f00);

	// leftarrow
	if (this->mainTable.currentPosition.x == 0)
		this->parent->setLedXY(5, 9, 0x003f0000);
	else
		this->parent->setLedXY(5, 9, 0x00003f00);

	// rightarrow
	if (this->mainTable.currentPosition.x + 8 == this->mainTable.getWidth())
		this->parent->setLedXY(6, 9, 0x003f0000);
	else
		this->parent->setLedXY(6, 9, 0x00003f00);

	return;
}

void
mk2Scene::restoreScene()
{
	this->restoreGrid();
}

void
mk2Scene::enter()
{
	this->restoreScene();
}

void
mk2Scene::leave()
{
	return;
}

bool
mk2Scene::wrapWidthToBoundary(uint32_t* x, uint32_t* y)
{
	uint32_t xcopy = *x;
	uint32_t ycopy = *y;
	uint32_t reminder = 0;

	if (!this->mainTable.isPointInBoundary(xcopy, ycopy)) {
		reminder = xcopy / this->mainTable.getWidth();
		xcopy %= this->mainTable.getWidth();
		ycopy += reminder;

		if (!this->mainTable.isPointInBoundary(xcopy, ycopy))
			return false;
	}

	*x = xcopy;
	*y = ycopy;
	return true;
}

void
mk2Scene::showTable()
{
	uint32_t color;
	uint32_t size = this->mainTable.getTotalFrameSize();
	for (uint32_t i = 0; i < size; i += 3) {
		if ((i % (this->mainTable.getWidth() * 3)) == 0)
			printf("\n");
		printf("[%02x %02x %02x]",
		       this->mainTable[i],
		       this->mainTable[i + 1],
		       this->mainTable[i + 2]);
	}
	printf("\n\n\n");

	for (auto i = this->mainTable.getHeight();
	     i != std::numeric_limits<decltype(i)>::max();
	     i--) {
		printf("[");
		for (uint32_t j = 0; j < this->mainTable.getWidth(); j++) {
			// Dont need to check return value
			// Upper checks in for loop are enough
			this->mainTable.dim3Get32(j, i, &color);
			printf("[%02x %02x %02x],",
			       static_cast<byte>(color >> 16),
			       static_cast<byte>(color >> 8),
			       static_cast<byte>(color));
		}
		printf("]\n");
	}
}

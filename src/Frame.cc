#include "Frame.h"

Frame::Frame(uint32_t width, uint32_t height)
{
	this->allocNewMemory(width, height);
}

Frame::Frame(const Frame& frame)
  : FrameBase() // Handle this manually
{
	if (this == &frame)
		return;

	this->currentPosition = frame.currentPosition;
	this->deallocMemory();

	// Copy new size and allocate new buffer according to it
	this->size = frame.size;
	this->data = new byte[this->getTotalFrameSize()];
}

Frame::~Frame()
{
	this->deallocMemory();
}

void
Frame::allocNewMemory(uint32_t width, uint32_t height)
{
	debug_print("Got res: {} {}\n", width, height);
	this->checkFixResolution(width, height);

	this->size.height = height;
	this->size.width = width;

	debug_printHL("Allocating memory for data {}x{} . . .", width, height);

	this->data = new byte[height * width * 3]();
	if (!this->data)
		debug_print("Error -- Could not allocate new memory!\n");

	debug_printHL(" OK -- Allocated in: [{}]\n", this->data);
}

void
Frame::deallocMemory()
{
	delete[] this->data;
}

int
Frame::resizeMemory(uint32_t width, uint32_t height)
{
	if (this->size.width == width && this->size.height == height)
		return -1;

	this->deallocMemory();
	this->allocNewMemory(width, height);

	this->currentPosition.x = 0;
	this->currentPosition.y = 0;

	return 0;
}

uint32_t
FrameBase::checkFixResolution(uint32_t& width, uint32_t& height)
{
	uint32_t ret = 0;

	if (width < DEFAULT_GRID_WIDTH) {
		width = 8;
		ret += 1;
	}

	if (height < DEFAULT_GRID_HEIGHT) {
		height = 8;
		ret += 1;
	}

	return ret;
}

bool
FrameBase::checkResolution(uint32_t width, uint32_t height)
{
	if (width < DEFAULT_GRID_WIDTH || height < DEFAULT_GRID_HEIGHT)
		return false;

	return true;
}

bool
FrameBase::isPointInBoundary(uint32_t x, uint32_t y) const
{
	if (x >= this->size.width || y >= this->size.height)
		return false;
	return true;
}

bool
FrameBase::dim3Set8(uint32_t x, uint32_t y, uint32_t z, byte value)
{
	auto offset =
	  (y * this->size.width * BYTES_PER_PIXEL) + (x * BYTES_PER_PIXEL);
	if (this->isPointInBoundary(x, y)) {
		*(this->data + offset + z) = value;
		return true;
	}
	debug_print("Out of range. Got coords: y:{} x:{}\n", y, x);
	return false;
}

bool
FrameBase::dim3Get8(uint32_t x, uint32_t y, uint32_t z, byte* value) const
{
	auto offset =
	  (y * this->size.width * BYTES_PER_PIXEL) + (x * BYTES_PER_PIXEL);
	if (this->isPointInBoundary(x, y)) {
		*value = *(this->data + offset + z);
		return true;
	}
	debug_print("Out of range. Got coords: y:{} x:{}\n", y, x);
	return false;
}

bool
FrameBase::dim3Set32(uint32_t x, uint32_t y, uint32_t value)
{
	auto offset =
	  (y * this->size.width * BYTES_PER_PIXEL) + (x * BYTES_PER_PIXEL);
	if (this->isPointInBoundary(x, y)) {
		*(this->data + offset) = static_cast<byte>(value >> 16);
		*(this->data + offset + 1) = static_cast<byte>(value >> 8);
		*(this->data + offset + 2) = static_cast<byte>(value);
		return true;
	}
	debug_print("Out of range. Got coords: y:{} x:{}\n", y, x);
	return false;
}

bool
FrameBase::dim3Get32(uint32_t x, uint32_t y, uint32_t* value) const
{
	auto offset =
	  (y * this->size.width * BYTES_PER_PIXEL) + (x * BYTES_PER_PIXEL);
	// (0,1) -> 0*9*3 + 1*3 = 3
	//			 -> 1*9*3 + 0*3 = 27
	if (this->isPointInBoundary(x, y)) {
		// LITTLE ENDIAN -- inserting these bytes backwards
		*(reinterpret_cast<byte*>(value) + 3) = 0;
		*(reinterpret_cast<byte*>(value) + 2) = *(this->data + offset);
		*(reinterpret_cast<byte*>(value) + 1) =
		  *(this->data + offset + 1);
		*(reinterpret_cast<byte*>(value)) = *(this->data + offset + 2);

		return true;
	}
	debug_print("Out of range. Got coords: y:{} x:{}\n", y, x);
	return false;
}

void
FrameBase::insertColorIfInBoundary(uint32_t x, uint32_t y, uint32_t color)
{
	if (this->isPointInBoundary(x, y)) {
		dim3Set32(x, y, color);
	}
}

void
FrameBase::insertTransparentableColorIfInBoundary(uint32_t x,
                                                  uint32_t y,
                                                  uint32_t color)
{
	if (this->isPointInBoundary(x, y) && color != 0) {
		dim3Set32(x, y, color);
	}
}

void
FrameBase::insertTransparentable8bitColorIfInBoundary(uint32_t x,
                                                      uint32_t y,
                                                      uint32_t color)
{
	color = utils::translate8to6BitColor(color);

	if (this->isPointInBoundary(x, y) && color != 0) {
		dim3Set32(x, y, color);
	}
}

#include "util/utils.h"
#include "types.h"

uint32_t
utils::translate8to6BitColor(uint32_t color)
{
	byte r;
	byte g;
	byte b;

	r = (byte)(color >> 16) / 4;
	g = (byte)(color >> 8) / 4;
	b = (byte)(color) / 4;

	color = r;
	color <<= 8;
	color |= g;
	color <<= 8;
	color |= b;

	return color;
}

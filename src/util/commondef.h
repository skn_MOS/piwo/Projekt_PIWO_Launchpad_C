#pragma once
#include <cstdint>
#include <array>
#include <string_view>

#include "util/utils.h"

// TODO(holz): move to some Mk2 specific header
static constexpr const int LP_GRID_WIDTH = 8;
static constexpr const int LP_GRID_HEIGHT = 8;
static constexpr const int LP_GRID_SIZE = LP_GRID_WIDTH * LP_GRID_HEIGHT;

enum class SCENEID : uint32_t
{
	DEFAULT = 0,
	MAINSCENEID = 0,
	COLORPICKSCENEID = 1,
	PRINTSCENEID = 2,
	GAMEPICKSCENEID = 3,
	TERMINATORSCENEID
};

struct SceneNameLookup
{
	constexpr static std::string_view names[underlay_cast(SCENEID::TERMINATORSCENEID)] {
		[underlay_cast(SCENEID::MAINSCENEID)] = "Main",
		[underlay_cast(SCENEID::COLORPICKSCENEID)] = "Colorpick",
		[underlay_cast(SCENEID::PRINTSCENEID)] = "Print",
		[underlay_cast(SCENEID::GAMEPICKSCENEID)] = "Game",
	};

	constexpr std::string_view operator[](size_t index) const
	{
		if (index >= underlay_cast(SCENEID::TERMINATORSCENEID))
			die("index >= SCENEID::TERMINATORSCENEID");
		return names[index];
	}

	constexpr std::string_view operator[](SCENEID id) const
	{
		return names[underlay_cast(id)];
	}

} constexpr sceneNameLookup;

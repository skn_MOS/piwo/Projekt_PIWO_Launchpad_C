#pragma once

#include <chrono>
#include <thread>

using time_ms_t = int64_t;
using time_duration_def_t = std::chrono::duration<time_ms_t, std::milli>;
using time_point_def_t =
  std::chrono::time_point<std::chrono::high_resolution_clock,
                          time_duration_def_t>;

#ifdef _WIN32
#include <windows.h>
#elif __linux__
#include <unistd.h>
inline void
Sleep(time_ms_t timeMs)
{
	std::this_thread::sleep_for(time_duration_def_t(timeMs));
}
#endif

// Keeps value of max fps counter
constexpr time_ms_t FPSLOCK = 40;
static_assert(FPSLOCK > 0);

constexpr auto MSPERFRAME = time_duration_def_t(1000 / 40);

inline time_point_def_t
getCurrentTime()
{
	return std::chrono::time_point_cast<time_duration_def_t>(
	  std::chrono::high_resolution_clock::now());
}

// If thread renderes to fast slows it down a bit
// Its better not to spam outpipe or launchpad driver
// It is necessary for other features to work properly
// Takes time when rendering of current frame started
inline void
fpslock(time_point_def_t startFrameTime)
{
	auto currentTime = getCurrentTime();
	startFrameTime += MSPERFRAME;
	auto time_to_sleep = startFrameTime - currentTime;
	if (time_to_sleep.count() > 0)
		std::this_thread::sleep_for(time_to_sleep);
}

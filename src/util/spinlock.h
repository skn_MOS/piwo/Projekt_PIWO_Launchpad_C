#pragma once
#include <atomic>

class spin_lock_t
{
public:
	spin_lock_t(std::atomic_flag& f)
	  : f(f)
	{
		this->lock();
	}

	~spin_lock_t() { this->unlock(); }

	void unlock() { this->f.clear(); }

	void lock()
	{
		while (this->f.test_and_set())
			;
	}

private:
	std::atomic_flag& f;
};

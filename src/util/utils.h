#pragma once

#include <filesystem>
#include <stdint.h>
#include <type_traits>

template<class... Ts>
struct overloaded : Ts...
{
	using Ts::operator()...;
};
template<class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

template<typename T, typename... Ts>
consteval bool
is_any_of()
{
	return (std::is_same_v<T, Ts> || ...);
};

#define SCOPED(var) if (var; 1)

#ifdef __WIN32
#define PATH_SEPARATOR "\\"
#else
#define PATH_SEPARATOR "/"
#endif

#define LIKELY(x) __builtin_expect((x), 1)
#define UNLIKELY(x) __builtin_expect((x), 0)

#define _always_inline __attribute__((always_inline))

#define Q(x) #x
#define QUOTE(x) Q(x)

[[noreturn]] inline void
die(const char* errm, int errc = 1)
{
	fputs(errm, stderr);
	exit(errc);
}

template<class T>
using utype = std::underlying_type_t<T>;

template<class T>
inline constexpr decltype(auto)
underlay_cast(T e)
{
	return static_cast<utype<T>>(e);
}

namespace utils {
inline bool
isPointInMainGrid(uint32_t x, uint32_t y)
{
	if (x > 7)
		return false;

	if (y > 7)
		return false;

	return true;
}

uint32_t
translate8to6BitColor(uint32_t color);

template<typename T>
inline std::filesystem::path
joinPaths(T&& path)
{
	return path;
}

template<typename T, typename... Args>
inline std::filesystem::path
joinPaths(T&& path, Args&&... args)
{
	return path / joinPaths(args...);
}
};

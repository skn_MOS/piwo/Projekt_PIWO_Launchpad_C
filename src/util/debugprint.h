#pragma once

#include <fmt/format.h>

constexpr auto LOG_DEF_FORMAT = "[{}][{}][L:{}] ";

#if defined(LPD_WITH_NCURSES)
#include <mutex>
#include <vector>

#include "util/utils.h"

struct Logger
{
	using MessageType = std::string;

	Logger() = default;
	~Logger() = default;

	template<typename... StringArgs>
	friend Logger& operator<<(Logger& logger, StringArgs&&... args)
	{
		SCOPED(std::unique_lock lck(logger.mtx))
		{
			logger.logHistory.push_back(
			  std::forward<StringArgs>(args)...);
		}

		return logger;
	}

	auto lock() { return std::unique_lock(this->mtx); }

	auto begin() const { return std::begin(this->logHistory); }
	auto cbegin() const { return std::cbegin(this->logHistory); }
	auto end() const { return std::end(this->logHistory); }
	auto cend() const { return std::cend(this->logHistory); }

	std::mutex mtx;
	std::vector<std::string> logHistory;
};

extern Logger globalLogger;
#endif

#if defined(LPD_WITH_NCURSES)
#define log_stdout(fmtstr, ...)                                                \
	{                                                                      \
		globalLogger << fmt::format(fmtstr, ##__VA_ARGS__);            \
	}
#define log_stderr(fmtstr, ...) log_stdout(fmtstr, ##__VA_ARGS__)

#else

#define log_stdout(fmtstr, ...)                                                \
	{                                                                      \
		fmt::print(LOG_DEF_FORMAT, __FILE__, __FUNCTION__, __LINE__);  \
		fmt::print(fmtstr, ##__VA_ARGS__);                             \
	}

#define log_stderr(fmtstr, ...)                                                \
	{                                                                      \
		fmt::print(                                                    \
		  stderr, LOG_DEF_FORMAT, __FILE__, __FUNCTION__, __LINE__);   \
		fmt::print(stderr, fmtstr, ##__VA_ARGS__);                     \
	}

#endif

#ifndef LOG_ERRORS
#define LOG_ERRORS 0

#define debug_print(...)                                                       \
	{}
#define debug_printHL(...)                                                     \
	{}
#define debug_printerr(...)                                                    \
	{}
#else

#define debug_print(fmtstr, ...)                                               \
	{                                                                      \
		if constexpr (LOG_ERRORS) {                                    \
			fmt::print(                                            \
			  LOG_DEF_FORMAT, __FILE__, __FUNCTION__, __LINE__);   \
			fmt::print(fmtstr, ##__VA_ARGS__);                     \
		}                                                              \
	}

#define debug_printHL(fmtstr, ...)                                             \
	{                                                                      \
		if constexpr (LOG_ERRORS) {                                    \
			fmt::print(fmtstr, ##__VA_ARGS__);                     \
		}                                                              \
	}

#define debug_printerr(fmtstr, ...)                                            \
	{                                                                      \
		if constexpr (LOG_ERRORS) {                                    \
			fmt::print(stderr,                                     \
			           LOG_DEF_FORMAT,                             \
			           __FILE__,                                   \
			           __FUNCTION__,                               \
			           __LINE__);                                  \
			fmt::print(stderr, fmtstr, ##__VA_ARGS__);             \
		}                                                              \
	}

#endif

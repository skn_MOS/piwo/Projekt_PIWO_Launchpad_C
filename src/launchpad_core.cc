#include "launchpad_core.hpp"
#include "util/utils.h"

#ifdef _WIN32

#include <algorithm>
#include <limits>
#include <optional>
#include <string_view>
#include <tuple>

template<typename T>
concept WindowsMidiStructure = is_any_of<T, MIDIINCAPS, MIDIOUTCAPS>();

template<typename MidiStructType>
requires WindowsMidiStructure<MidiStructType> static std::optional<UINT>
findMk2Device(uint32_t devCount)
{
	MidiStructType caps;
	uint32_t i = 0;
	while (i < devCount) {

		if constexpr (std::is_same_v<MidiStructType, MIDIINCAPS>)
			midiInGetDevCaps(i, &caps, sizeof(MidiStructType));
		else
			midiOutGetDevCaps(i, &caps, sizeof(MidiStructType));

		if (std::string_view(caps.szPname).find(MK2_SZPNAME) !=
		    std::string_view::npos)
			return i;

		++i;
	}

	return std::nullopt;
}

void
Mk2Core::init(ButtonActionHandler* handler)
{
	constexpr auto NOMIDI_MSG =
	  "There is no midi ( input or output ) device present."
	  " Are you sure Mk2 is plugged ?\n";

	this->hMidiDeviceIn = NULL;
	this->hMidiDeviceOut = NULL;

	uint32_t deviceInCount = midiInGetNumDevs();
	uint32_t deviceOutCount = midiOutGetNumDevs();

	std::optional inCaps = findMk2Device<MIDIINCAPS>(deviceInCount);
	if (!inCaps.has_value()) {
		fprintf(stderr, NOMIDI_MSG);
		return;
	}

	std::optional outCaps = findMk2Device<MIDIOUTCAPS>(deviceOutCount);
	if (!outCaps.has_value()) {
		fprintf(stderr, NOMIDI_MSG);
		return;
	}

	MMRESULT ret;

	ret = midiInOpen(&this->hMidiDeviceIn,
	                 inCaps.value(),
	                 (DWORD_PTR)MidiInputProc,
	                 (DWORD_PTR)handler,
	                 CALLBACK_FUNCTION);

	if (ret != MMSYSERR_NOERROR) {
		fprintf(stderr, "Cannot open midi device (Input)\n");
		goto Mk2_cleanup;
	}

	ret = midiOutOpen(&this->hMidiDeviceOut,
	                  outCaps.value(),
	                  (DWORD_PTR)MidiOutputProc,
	                  (DWORD_PTR)this,
	                  CALLBACK_FUNCTION);

	if (ret != MMSYSERR_NOERROR) {
		fprintf(stderr, "Cannot open midi device (Output)\n");
		goto Mk2_cleanup;
	}

	midiInStart(this->hMidiDeviceIn);

	// dont fall through into cleanup
	return;

// Its called when something goes wrong
Mk2_cleanup:
	if (hMidiDeviceIn != NULL)
		midiInClose(this->hMidiDeviceIn);

	if (hMidiDeviceOut != NULL)
		midiOutClose(this->hMidiDeviceOut);

	this->hMidiDeviceIn = NULL;
	this->hMidiDeviceOut = NULL;
}

int
Mk2Core::close()
{
	midiInStop(this->hMidiDeviceIn);
	midiInClose(this->hMidiDeviceIn);

	midiOutClose(this->hMidiDeviceOut);

	this->hMidiDeviceIn = NULL;
	this->hMidiDeviceOut = NULL;

	return 0;
}

void
Mk2Core::printMidiDevices(const uint32_t deviceInCount,
                             const uint32_t deviceOutCount) const
{
	MIDIINCAPS inCaps;
	MIDIOUTCAPS outCaps;

	if (deviceInCount == 0) {
		return;
	}

	printf("[LAUNCHPAD] midi input devices:\n");
	for (uint32_t i = 0; i < deviceInCount; i++) {
		midiInGetDevCaps(i, &inCaps, sizeof(MIDIINCAPS));
		printf("\t%d : name = %s\n", i, inCaps.szPname);
	}
	printf("\n");

	if (deviceOutCount == 0) {
		return;
	}

	printf("[LAUNCHPAD] midi output devices:\n");
	for (uint32_t i = 0; i < deviceOutCount; i++) {
		midiOutGetDevCaps(i, &outCaps, sizeof(MIDIOUTCAPS));
		printf("\t%d : name = %s\n", i, outCaps.szPname);
	}
	printf("\n");

	return;
}

void
Mk2Core::printMidiDevicesByID(const uint32_t inid,
                                 const uint32_t outid) const
{
	MIDIINCAPS inCaps;
	MIDIOUTCAPS outCaps;

	midiInGetDevCaps(inid, &inCaps, sizeof(MIDIINCAPS));
	printf("[LAUNCHPAD] Choosed devices: %s (input), ", inCaps.szPname);

	midiOutGetDevCaps(outid, &outCaps, sizeof(MIDIOUTCAPS));
	printf("%s (output)\n", outCaps.szPname);

	return;
}

void
Mk2Core::handleMMSYSERR(const MMRESULT ret) const
{
	switch (ret) {
		case MIDIERR_NOTREADY: {
			debug_print("Device not ready\n");
			break;
		}
		case MIDIERR_UNPREPARED: {
			debug_print("Device unprepared\n");
			break;
		}
		case MMSYSERR_INVALHANDLE: {
			debug_print("Device invalid handle\n");
			break;
		}
		case MMSYSERR_INVALPARAM: {
			debug_print("Device invalid param\n");
			break;
		}
		default:
			break;
	}
}

ssize_t
Mk2Core::write(std::basic_string_view<byte> s) const
{
	constexpr auto size_limit =
	  static_cast<decltype(s.size())>(std::numeric_limits<DWORD>::max());

	const auto data = s.data();
	const auto size = std::min(s.size(), size_limit);

	midiOutPacket.lpData = (char*)data;
	midiOutPacket.dwBufferLength = static_cast<DWORD>(size);
	midiOutPacket.dwBytesRecorded = static_cast<DWORD>(size);
	midiOutPacket.dwFlags = 0;
	midiOutPacket.dwOffset = 0;

	auto ret = midiOutPrepareHeader(this->hMidiDeviceOut,
	                                &this->midiOutPacket,
	                                sizeof(this->midiOutPacket));

	this->handleMMSYSERR(ret);

	ret = midiOutLongMsg(this->hMidiDeviceOut,
	                     &this->midiOutPacket,
	                     sizeof(this->midiOutPacket));

	this->handleMMSYSERR(ret);

	return size;
}
#elif __linux__
#include <fcntl.h>
#include <unistd.h>
#include <usb.h>

Mk2Core::Mk2Core(ButtonActionHandler* handler)
  : readingThread(&MidiInputReader, std::ref(*this))
{
	struct usb_bus* bus;
	struct usb_device* dev;
	usb_init();
	usb_find_busses();
	usb_find_devices();

	debug_print("Initalizing connecton with launchpad.\n");

	constexpr decltype(dev->descriptor.idVendor) lpVID = 0x1235;
	constexpr decltype(dev->descriptor.idProduct) lpPID = 0x0069;

	this->button_handler = handler;
	this->deviceDescriptorRead = -1;
	this->deviceDescriptorWrite = -1;

	for (bus = usb_busses; bus; bus = bus->next) {
		for (dev = bus->devices; dev; dev = dev->next) {
			if (dev->descriptor.idVendor == lpVID &&
			    dev->descriptor.idProduct == lpPID) {
				puts("Found midi device");
			}
		}
	}

#ifdef CONFIG_MOCK_MK2_READ
	const char* deviceNameRead = QUOTE(CONFIG_MOCK_MK2_READ);
#else
	const char* deviceNameRead = "/dev/mk2launchpad";
#endif

#ifdef CONFIG_MOCK_MK2_WRITE
	const char* deviceNameWrite = QUOTE(CONFIG_MOCK_MK2_WRITE);
#else
	const char* deviceNameWrite = "/dev/mk2launchpad";
#endif

	debug_print("Launchpad read descriptor at: {}\n", deviceNameRead);
	debug_print("Launchpad write descriptor at: {}\n", deviceNameWrite);

	bool error = false;

	if ((this->deviceDescriptorRead = open(deviceNameRead, O_RDONLY)) ==
	    -1) {
		debug_print("Failed to open mk2 device for reading.\n");
		error = true;
	}

	if ((this->deviceDescriptorWrite = open(deviceNameWrite, O_WRONLY)) ==
	    -1) {
		debug_print("Failed to open mk2 device for writing.\n");
		error = true;
	}

	if (error) {
		fprintf(stderr,
		        "There is no midi ( input or output ) device present."
		        " Are you sure Mk2 is plugged ?\n");
		exit(-1);
	}
}

ssize_t
Mk2Core::write(std::basic_string_view<byte> s) const
{
	const auto data = s.data();
	const auto size = s.size();

	return ::write(this->deviceDescriptorWrite, data, size);
}

Mk2Core::~Mk2Core()
{
	::close(this->deviceDescriptorRead);
	::close(this->deviceDescriptorWrite);
	this->readingThread._terminate();
}

#endif

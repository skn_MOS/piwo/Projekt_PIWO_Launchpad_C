#pragma once

#include <array>
#include <list>
#include <mutex>
#include <optional>
#include <string.h>

#include "RenderObject.h"
#include "SuspendableThread.hpp"
#include "util/time.h"
#include "util/utils.h"
#include "util/spinlock.h"

// TODO(holz) update --> callback is out !
// Class for managing rendering core that was returned from renderengine
// rendercore is supposed to render objects on its canvas at proper moment
// and offset and when the frame is ready rendercore will callback the
// function supplied by its owner during creation of the core.
// Cores are resources owned by mk2Scene object and configured
// to fullfill needs of its owner. Rendercore can be stopped
// by using suspendthread and resumed by resumethread functions
// both stops thread of creating next frame, but creating of current
// must end. For being sure that call to suspendthread retunrs when
// core actually stoped suspendAndWaitThread/resumeAndWaitThread should be used
template<typename MasterType_>
class RenderEngineCore
{
	// Types
public:
	using MasterType = MasterType_;

	// Having optional objects in an array isn't probably the most
	// efficient, but we expect this array to be small (8 - 32 elements).
	using render_object_list_t = std::array<std::optional<RenderObject>, 32>;

protected:
	// Handle to a thread that runs main render loop
	SuspendableThread renderLoopHandle;

	// Renders thread will render to the canvas
	SharedFrame canvas;

	// Temporary canvas used to prepare final frame.
	// When its ready it is copied into canvas.
	// This mechanism is used minimize black flickering when player
	// (or other user) reads frame from shared buffer during rendering.
	Frame canvas_tmp;

	// Instead of clearing frame to generate new
	// frame will be filled with background from here
	// Its allows renderthread client to have static background
	// under frame that is being rendered with objects
	const Frame* canvasBackground;

	// Holds RenderObjects. Used in main render loop that iterates through
	// and renders next frames of next objects in list to the main memory of
	// this class
	render_object_list_t renderList;
	std::atomic_flag renderListSpinLock;

private:
	// Copies static background to canvas
	void copyBackgroundToCanvas()
	{
		memcpy(this->canvas_tmp.getData(),
		       this->canvasBackground->getData(),
		       this->canvas_tmp.getTotalFrameSize());
	}

public:
	explicit RenderEngineCore(const Frame* canvasBackground,
	                          std::string_view name)
	  : renderLoopHandle(&renderLoop, std::ref(*this))
	  , canvas(name,
	           canvasBackground->getWidth(),
	           canvasBackground->getHeight())
	  , canvas_tmp(canvasBackground->getWidth(),
	               canvasBackground->getHeight())
	  , canvasBackground(canvasBackground)
	{}

	~RenderEngineCore() { this->killRenderThread(); }

	// Sets flag for renderthread to stop rendring
	// and waits until renderthread stops
	void suspendAndWaitRenderThread()
	{
		this->renderLoopHandle.stopAndWait();
	}

	// Sets flag for renderthread to resume rendering
	// and waits util renderthread resumes
	void resumeAndWaitRenderThread()
	{
		this->renderLoopHandle.resumeAndWait();
	}

	// Simply sets flag for renderthread to suspend execution
	// WARNING: if you want to be sure that after this call renderthread
	//					is for sure suspended call
	// suspendAndWaitRenderThread
	void suspendRenderThread() { this->renderLoopHandle.stop(); }

	void killRenderThread() { this->renderLoopHandle.kill(); }

	// Simply sets flag for renderthread to resume execution
	// WARNING: if you want to be sure that after this call renderthread
	//					is for sure resumed call
	// resumeAndWaitRenderThread
	void resumeRenderThread()
	{
		if (!this->canvas.shmem_valid()) {
			debug_print("Canvas is null but tried to start "
			            "rendering on it!\n");
			return;
		}

		this->renderLoopHandle.resumeAndWait();
	}

	// Safetly updates information about
	// background canvas which renderthread uses
	void updateCanvasBackgroundInfo(const Frame* background)
	{
		bool isRenderingcopy = this->renderLoopHandle.isStopped();

		this->suspendAndWaitRenderThread();

		this->canvasBackground = background;

		this->canvas.resizeMemory(this->canvasBackground->getWidth(),
		                          this->canvasBackground->getHeight());

		this->canvas_tmp.resizeMemory(
		  this->canvasBackground->getWidth(),
		  this->canvasBackground->getHeight());

		if (!isRenderingcopy)
			this->resumeAndWaitRenderThread();
	}

	// Calls printme for all renderable objects existing
	void printAllObjectsInfo()
	{
		size_t objs = 0;

		for (auto& p : this->renderList) {
			if (p.has_value()) {
				p.value().printMe();
				++objs;
			}
		}

		if (objs == 0) {
			log_stdout("Render list is empty.\n");
		}
	}

	// Inserts new render object to render list
	void spawnNewObject(const RenderObject &robj)
	{
		spin_lock_t lock(this->renderListSpinLock);

		for (auto& p : this->renderList) {

			if (!p.has_value()) {
				p.emplace(robj);
				break;
			}
		}
	}

	auto isRendering()
	{
		return !this->renderLoopHandle.isStopped();
	}

	auto isStopRequested()
	{
		return this->renderLoopHandle.isStopRequested();
	}

	const decltype(canvas)& getFrame() { return this->canvas; }

	static _always_inline void renderLoop(
	  RenderEngineCore& rec)
	{
		auto master = (MasterType*)(&rec);

		// Gets current time. Used to calc fps lock duration.
		auto startFrameTime = getCurrentTime();

		rec.copyBackgroundToCanvas();

		SCOPED(spin_lock_t lock(rec.renderListSpinLock))
		{
			// iterates through all to render elements
			// Main rendering loop
			for (auto it = rec.renderList.begin();
			     it != rec.renderList.end();
			     ++it) {

				if (!it->has_value() ||
				    it->value().hasAnimationEnded()) {
					*it = std::nullopt;
					continue;
				}

				// Renders object to scenes memory using its
				// method for inserting transparent colors
				it->value().flushFrame(rec.canvas_tmp);
			}
		}

		copyFrame(rec.canvas, rec.canvas_tmp);
		master->renderCallback(&rec.canvas);

		// Slows down rendering.
		fpslock(startFrameTime);
	}
};

#ifndef _MK2SCENE_H_
#define _MK2SCENE_H_

// Used to simplify usage of mainTable
#define dim3Ref(mem, width, y, x, z) *(mem + ((y) * (width)*3) + ((x)*3) + z)

#include "Frame.h"
#include "lpPacketTemplates.h"

#include "IInputHandler.h"

#include "cli.hpp"

#include <stdint.h>
#include <stdio.h>
typedef unsigned char byte;

// Negative value means error
typedef int cli_status_t;

namespace cli {
constexpr cli_status_t NOT_IMPLEMENTED = -2;
constexpr cli_status_t QUIT = -1;
}

class Mk2;
struct mk2Button;

// Every scene object should inherit after this class
// and overwrite its handleButtonPressed function
// that will be called by launchpad object.
// Current scene that is activated will determine which
// object's from scenes table ( table of scene objects )
// method to call when user presses button on
// launchpad keyboard.
class mk2Scene
  : public ButtonActionHandler
  , public cliiface
{
protected:
	// Pointer to parent launchpad class
	Mk2* parent;

	// Translates xy coord to wrap inside boundary of calling object main
	// memory If such operation is possible translates overflowing x into
	// number of width's to y. But if after or before such operation height
	// is overflowing returns 0 If operation succed fills x and y params
	// with new values within boundary
	bool wrapWidthToBoundary(uint32_t* x, uint32_t* y);

	// Clears all buttons of launchpad given in argument
	// It calls setLedAll but Additionally saves new (empty) state
	void clearPad();

public:
	// Each inheriting object should have its memory
	// which is representation of launchpads keyboard.
	// Its used to save states of buttons.
	Frame mainTable;

	// Takes size of maintable as parameters
	// first parameter is X cord (width)
	// second parameter is Y cord (height)
	// Allocates 3 dimensional array
	// 2 dims are coord system of buttons
	// and 3rd is 3 bytes that describes RGB values of light on button
	// Last argument is pointer to parent launchpad that creates scene
	mk2Scene(uint32_t width, uint32_t height, Mk2* parent);

	// Deallocates main 3 dimensional array
	virtual ~mk2Scene();

	// Flushes main table to launchpad
	void flushMainTableToLaunchpad() const;

	// Deallocs and reallocs new main memory in size described by
	// passed parameters
	virtual int reallocMainMem(uint32_t width, uint32_t height);

	// Will be overwritten by given scene object
	// to deliver appropriate event to currently
	// selected scene.
	virtual void handleButtonPressed(const mk2Button& button) = 0;

	// Manager of scenes calls this method when switching to this scene.
	// Scene should restore all the necessary resources it need to properly
	// run.
	virtual void enter();

	// Manager of scenes calls this method when switching to another scene.
	// Currently running scene should do necessary preparation for exit,
	// like stopping all slave threads, unlocking/locking mutexes, stopping
	// IO etc.
	virtual void leave();

	void restoreScene();

	// Restores pixels to main memory of mainScene that are staticly colored
	// pixels
	void restoreStaticColors();

	// Prints content of maintable. Used for debug.
	void showTable();

	// Restores colors of main 8x8 grid and colors of function buttons
	// Can be overwritten by child. Its called by launchpad when switchScene
	// is called to restore button state for particular scene
	void restoreGrid();

	// Checks and restores colors on arrow keys
	void restorePositionState();

	virtual std::string_view getStatus() { return ""; }

	virtual std::string_view getName() const = 0;
};

#endif // _MK2SCENE_H_

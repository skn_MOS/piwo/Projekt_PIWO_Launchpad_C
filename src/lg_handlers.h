#pragma once
#include <array>
#include <cstdint>

class mk2MainScene;

constexpr size_t lg_local_buff_size = 4;
using lg_local_buf_t = std::array<uint32_t, lg_local_buff_size>;

template<typename T>
constexpr T lg_ip_resize_total_size = static_cast<T>(3 * sizeof(uint32_t));

void
lg_ip_nop_handler(mk2MainScene* ms);
void
lg_ip_resize_handler(mk2MainScene* ms, lg_local_buf_t buff, uint32_t read);
void
lg_ip_accept_handler(mk2MainScene* ms);
void
lg_ip_reject_handler(mk2MainScene* ms);
void
lg_ip_end_handler(mk2MainScene* ms);

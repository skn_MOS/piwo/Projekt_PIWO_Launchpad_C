#pragma once
#include <cstdint>

#include "types.h"
#include "util/utils.h"

// Structure that wraps information about signle launchapd button.
// XY variable is xy coordinates packed
// into char type. Most significant nibble is X and least is Y.
// State is set to 1 or 0 accordingly whether button
// has been pressed or released
struct mk2Button
{
	byte XY;
	byte state;
};

enum class Mk2Color : byte
{
	black = 0x00,
	yellow = 0x7C,
	green = 87,
	purple = 0x36,
	blue = 0x43,
	red = 120,
	white = 0x03,
};

enum class Mk2Speed : unsigned char
{
	vslow = 0x01,
	slow = 0x03,
	medium = 0x05,
	fast = 0x07,
};

template<typename T>
struct Position
{
	using underlying_type = T;
	T x;
	T y;
};

using ipos_t = Position<int32_t>;
using upos_t = Position<uint32_t>;

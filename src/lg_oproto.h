// ==== ++++ ====
// OUTPUT LAUNCHPAD -> GUI PROTOCOL LIBRARY
// version 3.0
// This is implementation of launchpad to GUI PROTOCOL
// documented in SKN MOS DOCS
// ==== ++++ ====

#pragma once
#include <cstdint>

using lg_oproto_t = uint32_t;

constexpr lg_oproto_t LG_OP_NOP = 0x40;
constexpr lg_oproto_t LG_OP_RESIZE = 0x41;
constexpr lg_oproto_t LG_OP_ACCEPT = 0x42;
constexpr lg_oproto_t LG_OP_REJECT = 0x43;
constexpr lg_oproto_t LG_OP_SHMRELOAD = 0x44;

#include "launchpad.h"
#include <mipc/base.h>

int32_t
main(void)
{
	mipc::init();

#ifdef LPD_WITH_NCURSES
	initscr();
#endif

	Mk2 lp;
	lp.cli();

#ifdef LPD_WITH_NCURSES
	endwin();
#endif
	return 0;
}

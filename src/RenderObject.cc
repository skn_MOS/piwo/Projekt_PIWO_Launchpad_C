#include "RenderObject.h"

RenderObject::RenderObject(uint32_t xcoord,
                           uint32_t ycoord,
                           piwo::animation_view anim)
  : offset({ xcoord, ycoord })
  , nextFrameTime()
  , animation(anim)
  , current_frame(this->animation.begin())
  , animationEnded(false)
{
	if (this->current_frame == this->animation.end())
		this->animationEnded = true;
}

void
RenderObject::flushFrame(FrameBase& frame)
{
	const auto anim_height =
	  static_cast<uint32_t>(this->animation.height());
	const auto anim_width = static_cast<uint32_t>(this->animation.width());

	// Offsets for spawining animation to spawn it always on the place of
	// click
	auto xoffset = this->offset.x;
	auto yoffset = this->offset.y;

	// Making it points to middle
	xoffset -= anim_width / 2;
	yoffset -= anim_height / 2;

	// If its time to render new frame, shift pointer to next frame first
	if (this->isTimeToRender()) {

		++this->current_frame;

		if (this->current_frame == this->animation.end()) {
			this->animationEnded = true;
			return;
		}

		// Time now
		this->nextFrameTime = getCurrentTime();

		// time that indicates frame render
		this->nextFrameTime +=
		  time_duration_def_t(this->current_frame->delay);
	}

	auto y = anim_height;
	while (y != 0) {
		--y;
		for (decltype(y) x = 0; x < anim_width; x++) {
			auto color =
			  this->current_frame->at_(x, y).convert<uint32_t>();
			frame.insertTransparentable8bitColorIfInBoundary(
			  x + xoffset, y + yoffset, color);
		}
	}
}

bool
RenderObject::hasAnimationEnded()
{
	return this->animationEnded;
}

void
RenderObject::printMe()
{
	const auto animation_size =
	  this->animation.frame_count() * this->animation.frame_offset();
	log_stdout("Offset: {}x{}"
	           "\n\tHas ended: {}"
	           "\n\tFrame size: {}x{}"
	           "\n\tSize of anim: {}"
	           "\n\tNext frame at: {}"
	           "\n\tAddress: {}\n",
	           this->offset.x,
	           this->offset.y,
	           this->hasAnimationEnded(),
	           this->animation.width(),
	           this->animation.height(),
	           animation_size,
	           this->nextFrameTime.time_since_epoch().count(),
	           (void*) this->animation.data());
}

bool
RenderObject::isTimeToRender()
{
	return (getCurrentTime() > this->nextFrameTime);
}

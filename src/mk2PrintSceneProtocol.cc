#include "launchpad.h"
#include "mk2PrintScene.h"
#include <string_view>

void
mk2PrintScene::print(std::string_view s) const
{
	this->parent->argStack.push(reinterpret_cast<intptr_t>(&s));
	this->parent->switchScene(SCENEID::PRINTSCENEID);

	// FIXME:
	// This sleep is necessary due to poor implementation of Windows driver
	// for MK2. PrintScene has to way of getting the packet from MK2 that
	// text scrolling has ended so we sleep for 5s (approx. time for 4 digit
	// score to scroll).
#if defined(__WIN32)
	const double strSize = static_cast<double>(s.size());
	const double scrollTimePerChar = 0.64;
	const double scrollTimeConst = 1; // Constatnt delay for all prints

	const auto scrollTime = std::chrono::seconds(
	  static_cast<size_t>(strSize * scrollTimePerChar + scrollTimeConst));

	debug_print("Sleeping for {}\n", scrollTime.count());
	std::this_thread::sleep_for(scrollTime);
	this->parent->switchScene(SCENEID::PRINTSCENEID);
#endif
}

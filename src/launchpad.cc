#include "launchpad.h"
#include "RenderObject.h"

#include "lpPacketTemplates.h"
#include "util/spinlock.h"
#include "util/utils.h"

#include <cstring>
#include <iostream>
#include <iterator>
#include <optional>

#include <fmt/format.h>

#if defined(LPD_WITH_NCURSES)
#include <ncurses.h>
#endif

Mk2::Mk2()
  : Mk2Core(this)
  , currentScene(SCENEID::DEFAULT)
#if defined(LPD_OMIT_REDUNDANT_WRITES)
  , mirrorFrame(LP_GRID_WIDTH, LP_GRID_HEIGHT)
#endif
  , mainScene(std::make_unique<mk2MainScene>(this, 20, 20))
  , colorpickScene(std::make_unique<mk2ColorpickScene>(9, 9, this))
  , printScene(std::make_unique<mk2PrintScene>(8, 8, this))
  , gameScene(std::make_unique<mk2GameScene>(8, 8, this))
  , scenes{
	  [underlay_cast(SCENEID::MAINSCENEID)] =
	    static_cast<mk2Scene*>(this->mainScene.get()),

	  [underlay_cast(SCENEID::COLORPICKSCENEID)] =
	    static_cast<mk2Scene*>(this->colorpickScene.get()),

	  [underlay_cast(SCENEID::PRINTSCENEID)] =
	    static_cast<mk2Scene*>(this->printScene.get()),

	  [underlay_cast(SCENEID::GAMEPICKSCENEID)] =
	    static_cast<mk2Scene*>(this->gameScene.get()),
  }
{
#if !defined(LPD_WITH_NCURSES)
	this->printSKNMOS();
#endif

	mk2PInit mk2InitFrame;
	auto [data, size] = mk2InitFrame.serialize();
	this->write({ data, size });

	this->switchScene(SCENEID::MAINSCENEID);

#ifdef __linux__
	this->readingThread.resume();
#endif
}

// bool
// Mk2::setLedB(const byte XY, const uint32_t color)
//{
//	this->setLedXYFrame.xy = XY;
//	this->setLedXYFrame.r = (color & 0x00ff0000) >> 16;
//	this->setLedXYFrame.g = (color & 0x0000ff00) >> 8;
//	this->setLedXYFrame.b = color & 0x000000ff;
//
//	auto [data, size] = this->setLedXYFrame.serialize();
//
//	this->write(data, size);
//
//	return true;
//}

bool
Mk2::setLedAll(Mk2Color color)
{
	mk2PSetLedAll setLedAllFrame;
	setLedAllFrame.color = underlay_cast(color);

	auto [data, size] = setLedAllFrame.serialize();
	this->write({ data, size });

	return true;
}

bool
Mk2::setString(std::basic_string_view<byte> str, Mk2Color c, Mk2Speed s) const
{
	mk2PSetString setStringFrame;
	const auto stringSize = str.size();

	if (stringSize > mk2PSetString::maxStringLen) {
		debug_print("Tried to write too long string");
		return false;
	}

	setStringFrame.clear();
	setStringFrame.color = underlay_cast(c);
	setStringFrame.loop = 0;
	setStringFrame.speed = underlay_cast(s);
	setStringFrame.append(str);

	auto [data, size] = setStringFrame.serialize();
	this->write({ data, size });

	return true;
}

bool
Mk2::setLedXY(const uint32_t x, const uint32_t y, const uint32_t color)
{
	mk2PSetLedXY setLedXYFrame;
	byte XY = static_cast<byte>(y * 10 + x + 0x0b);

	setLedXYFrame.xy = XY;
	setLedXYFrame.r = (color & 0x00ff0000) >> 16;
	setLedXYFrame.g = (color & 0x0000ff00) >> 8;
	setLedXYFrame.b = color & 0x000000ff;

	auto [data, size] = setLedXYFrame.serialize();
	this->write({ data, size });

	return true;
}

bool
Mk2::pulseLedXY(const uint32_t x, const uint32_t y, Mk2Color c)
{
	mk2PPulseLedXY pulseLedXYFrame;
	byte XY = static_cast<byte>(y * 10 + x + 0x0b);

	pulseLedXYFrame.xy = XY;
	pulseLedXYFrame.color = underlay_cast(c);

	auto [data, size] = pulseLedXYFrame.serialize();
	this->write({ data, size });

	return true;
}

bool
Mk2::setGridLedXY(const uint32_t x, const uint32_t y, const uint32_t color)
{
	if (x < 8 && y < 8)
		return this->setLedXY(x, y, color);

	return false;
}

void
Mk2::flushFrameToLaunchpadAll(const FrameBase& frame) const
{
	mk2PSetLedXY setLedXYFrame;
	mk2PUpdatePacked updatePacked;

	uint32_t color;
	auto updatePackedIt = updatePacked.begin();
	auto xyframe_beg = reinterpret_cast<byte*>(&setLedXYFrame);
	auto xyframe_end = xyframe_beg + sizeof(setLedXYFrame);

	for (uint32_t i = 0; i < 8; i++) {
		for (uint32_t j = 0; j < 8; j++) {
			frame.dim3Get32(
			  frame.translateX(j), frame.translateY(i), &color);

			byte XY = static_cast<byte>(i * 10 + j + 0x0b);
			setLedXYFrame.xy = XY;
			setLedXYFrame.r = (color & 0x00ff0000) >> 16;
			setLedXYFrame.g = (color & 0x0000ff00) >> 8;
			setLedXYFrame.b = color & 0x000000ff;

			// Copy elements and update iterator
			updatePackedIt =
			  std::copy(xyframe_beg, xyframe_end, updatePackedIt);
		}
	}

	this->write(
	  { updatePacked.data(),
	    static_cast<uint32_t>(updatePackedIt - updatePacked.cbegin()) });
}

#if defined(LPD_OMIT_REDUNDANT_WRITES)

void
Mk2::flushFrameToLaunchpadSkipRedundant(const FrameBase& frame)
{
	mk2PUpdatePacked updatePacked;
	const bool forceDraw = this->forceNextRedraw;
	uint32_t colorSrc, colorDst;
	const mk2PSetLedXYBulkHeader header;
	mk2PSetLedXYFrameBulkPayload payload;

	using SetLedXYBulkFooter = mk2PacketFooter;

	auto updatePackedIt = updatePacked.begin();
	auto xyframe_beg = reinterpret_cast<byte*>(&payload);
	auto xyframe_end = xyframe_beg + sizeof(payload);

	// Make sure the local buffer is big enough to hold the bulk packet at
	// "worst case" - where we have to update whole grid.
	static_assert(sizeof(updatePacked) >=
	              sizeof(header) +
	                LP_GRID_SIZE * sizeof(payload) +
	                sizeof(SetLedXYBulkFooter));

	updatePackedIt = std::copy_n(reinterpret_cast<const byte*>(&header),
	                             sizeof(header),
	                             updatePackedIt);

	for (uint32_t i = 0; i < LP_GRID_HEIGHT; i++) {
		for (uint32_t j = 0; j < LP_GRID_WIDTH; j++) {

			const auto xSrc = frame.translateX(j);
			const auto ySrc = frame.translateY(i);
			frame.dim3Get32(xSrc, ySrc, &colorSrc);

			const auto xDst = mirrorFrame.translateX(j);
			const auto yDst = mirrorFrame.translateY(i);
			mirrorFrame.dim3Get32(xDst, yDst, &colorDst);

			// Update
			mirrorFrame.dim3Set32(xDst, yDst, colorSrc);

			// Skip same colors
			if (LIKELY(colorSrc == colorDst && !forceDraw))
				continue;

			// Prepare packet
			byte XY = static_cast<byte>(i * 10 + j + 0x0b);
			payload.xy = XY;
			payload.r = (colorSrc & 0x00ff0000) >> 16;
			payload.g = (colorSrc & 0x0000ff00) >> 8;
			payload.b = colorSrc & 0x000000ff;

			// Copy packet and update iterator
			updatePackedIt =
			  std::copy(xyframe_beg, xyframe_end, updatePackedIt);

			if (UNLIKELY(updatePackedIt >= updatePacked.end())) {
				log_stderr("INTERNAL ERROR: buffer for bulk update too small!");
				return;
			}
		}
	}

	// Nothing to send really
	if (updatePackedIt == updatePacked.cbegin() + sizeof(header))
		return;

	// Terminate bulk with footer
	assert(updatePackedIt + sizeof(mk2PacketFooter) <= updatePacked.end());
	const mk2PacketFooter footer;
	updatePackedIt = std::copy_n(reinterpret_cast<const byte*>(&footer),
	                             sizeof(footer),
	                             updatePackedIt);

	// Write all packets at once
	this->write(
	  { updatePacked.data(),
	    static_cast<uint32_t>(updatePackedIt - updatePacked.cbegin()) });
}

#endif

void
Mk2::flushFrameToLaunchpad(const FrameBase& frame)
{
#if defined(LPD_OMIT_REDUNDANT_WRITES)
	this->flushFrameToLaunchpadSkipRedundant(frame);

	// If it was true then we have just redrawn everything so set it to
	// false. If it was already false then setting it again doesn't change
	// anything, so do it everytime to save condition check here.
	this->forceNextRedraw = false;
#else
	this->flushFrameToLaunchpadAll(frame);
#endif
}

void
Mk2::switchScene(SCENEID scene)
{
	if (this->currentScene == scene)
		scene = this->sceneStack.pop();
	else
		this->sceneStack.push(this->currentScene);

	this->scenes[underlay_cast(this->currentScene)]->leave();
	this->scenes[underlay_cast(scene)]->enter();
	this->currentScene = scene;
	log_stdout("Switching to {}\n", underlay_cast(this->currentScene));
}

void
Mk2::handleButtonPressed(const struct mk2Button& button)
{
	debug_print("Handle button pressed MASTER\n");

	// dont decode sigle tap twice
	// second frame will be sent after release
	// just ignore release
	if (!button.state)
		return;

	scenes[underlay_cast(this->currentScene)]->handleButtonPressed(button);
}

bool
Mk2::isGrid(const mk2Button& btn) const
{
	if ((btn.XY >> 4 < 8) && ((btn.XY & 0x0f) < 8))
		return true;
	return false;
}

void
Mk2::handlePiwo7(const char* name)
{
	printf("FAKE HANDLING PIWO7 %s\n", name);
}

#if defined(LPD_WITH_NCURSES)

void
Mk2::drawStatus(std::string_view errmsg) const
{
	CliWindow &infoWindow = this->drawContext.infoWindow,
	          &logWindow = this->drawContext.logWindow,
	          &commandWindow = this->drawContext.commandWindow;

	clear();
	refresh();
	infoWindow.reset();
	logWindow.reset();
	commandWindow.reset();

	this->drawContext.drawHeader();

	// Draw native info
	infoWindow.drawLine("Current scene: {}",
	                    sceneNameLookup[this->currentScene]);

	std::array<std::string_view, sceneCount> sceneNames;

	for (uint32_t i = 0; i < sceneCount; ++i)
		sceneNames[i] = this->scenes[i]->getName();

	infoWindow.drawLine("Loaded scenes: {}", fmt::join(sceneNames, ", "));
	infoWindow.drawLine("Animations count: {}", this->sceneCount);

	// mk2mainscene "inject section"
	infoWindow.drawLine("Color: {}", this->mainScene->getCurrentColor());

	const auto pos = this->mainScene->mainTable.currentPosition;
	infoWindow.drawLine("Pos: X={}, Y={}", pos.x, pos.y);

	const auto size = this->mainScene->mainTable.getSize();
	infoWindow.drawLine("Size: {} x {}", size.width, size.height);

	const auto shmem_sharing = this->mainScene->shmem_valid();
	if (shmem_sharing) {
		infoWindow.drawLine("Sharing memory: YES");
		infoWindow.drawLine("Buffer name: {}",
		                    this->mainScene->shmem_name());
	} else {
		infoWindow.drawLine("Sharing memory: NO");
	}

	// log window
	SCOPED(auto lock = globalLogger.lock())
	{
		for (const auto& m : globalLogger)
			logWindow.drawLine(m);
	}

	if (errmsg != "")
		commandWindow.drawLine(errmsg);
	else
		commandWindow.skipLine();

	commandWindow.draw("> ");
	Position commandPos = commandWindow.getPos();
	move(commandPos.y, commandPos.x);

	infoWindow.drawBorder();
	logWindow.drawBorder();
	commandWindow.drawBorder();

	infoWindow.refresh();
	logWindow.refresh();
	commandWindow.refresh();
}

struct CommandParseResult
{
	std::string_view callee, command, args;
};

static std::optional<CommandParseResult>
ParseCommandInvokation(std::string_view input)
{
	const auto dotpos = std::find(std::begin(input), std::end(input), '.');
	if (dotpos == std::end(input))
		return std::nullopt;

	const auto calleeLen = std::distance(input.data(), dotpos);
	assert(calleeLen > 0);
	const std::string_view callee(input.begin(),
	                              static_cast<size_t>(calleeLen));

	// Its ok if there is no space - no args then.
	const auto spacepos = std::find(dotpos + 1, std::end(input), ' ');

	const auto commandLen = std::distance(dotpos + 1, spacepos);
	assert(commandLen >= 0);
	const std::string_view command(dotpos + 1,
	                               static_cast<size_t>(commandLen));

	const auto argsLen = std::distance(spacepos, std::end(input));
	assert(argsLen >= 0);
	std::string_view args(spacepos + 1, static_cast<size_t>(argsLen));

	return CommandParseResult{ .callee = callee,
		                   .command = command,
		                   .args = args };
}

#endif

void
Mk2::cli()
{
#ifdef LPD_WITH_NCURSES
	using namespace std::chrono_literals;
	clear();
	refresh();

	constexpr static size_t MAX_COMMAND_SIZE = 64;
	char command[MAX_COMMAND_SIZE];
	std::string lastError;

	drawStatus("");

	for (;;) {
		drawStatus(lastError);

		if (getnstr(command, static_cast<int>(MAX_COMMAND_SIZE)) == ERR)
			break;

		if (strncmp(command, "quit", MAX_COMMAND_SIZE) == 0)
			break;

		if (!*command) {
			lastError = "";
			continue;
		}

		std::optional parseResult = ParseCommandInvokation(command);

		if (!parseResult) {
			lastError =
			  "Usage: <scene_name>.<command_name> [<arg1> "
			  "[<arg2> ... [<argN>]]]";

			continue;
		}

		auto& [callee, command, args] = parseResult.value();

		mk2Scene* scene = nullptr;
		for (auto* s : scenes) {
			if (callee == s->getName())
				scene = s;
		}

		if (!scene) {
			lastError = "Error: there is no such scene";
			continue;
		}

		if (command == "help") {
			scene->printCLI();
			continue;
		}

		CliResult result = scene->cli(command, args);

		if (std::holds_alternative<CliResultOk>(result))
			lastError = "Ok";
		else
			lastError =
			  std::move(std::get<CliResultError>(result).error);
	}
#else
	this->scenes[underlay_cast(SCENEID::MAINSCENEID)]->cli();
#endif
}

void
Mk2::printSKNMOS()
{
	puts("~-~-~-~ SKN MOS SOFTWARE ~-~-~-~");
}

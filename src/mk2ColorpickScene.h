#ifndef _MK2COLORPICKSCENE_H_
#define _MK2COLORPICKSCENE_H_

#include "mk2Scene.h"
#include <stdio.h>

class Mk2;
class mk2MainScene;
struct mk2Button;

// Class for main scene that is used to color buttons ( windows )
// Erase single or all buttons ( windows )
// Inherits from virtual mk2Scene and overwrites appropriately
// handleButtonPressed method to deliver behavior for this
// particular scene
class mk2ColorpickScene final : public mk2Scene
{
public:
	// Calls parent constructor with same arguments as passed to it
	mk2ColorpickScene(uint32_t, uint32_t, Mk2* parent);

	// For this particular class handleButtonPressed will
	// color each clicked button on currently selected color
	// If top right button is pressed it will change to scene
	// where you select new color for painting
	// Eraser should be just below color picking
	// Clear whole pad functionality should be on most bottom
	// right functional button.
	// All changes to buttons need to be performed via connection
	// that is already established, thats why main launchpad is
	// passed in argument
	virtual void handleButtonPressed(const mk2Button& button) override;

	virtual void enter() override;
	virtual void leave() override;

	// Restores all features of scene
	// for this particular rerun whole loop of generating palette
	void restoreScene();

	// Generating new palette and
	// filling mainTable memory with generated palette
	void generatePalette();

	// Helper function to set middle buttons to chess like colors
	// to provide black and white colors to palette
	void makeMiddleChessy();

	// Friend of mk2MainScene class, can change its private
	// member - current color
	void changeMainsCurrentColor(uint32_t color, mk2MainScene* ms);
	// Additionally casts to mk2Mainscene ptr
	void changeMainsCurrentColor(uint32_t color, mk2Scene* ms);

	virtual std::string_view getStatus() override
	{
		return "ColorPickScene: all good";
	}

	virtual std::string_view getName() const override
	{
		return "ColorpickScene";
	}
};

#endif

#pragma once

class ButtonActionHandler
{
public:
	virtual void handleButtonPressed(const struct mk2Button& button) = 0;
};

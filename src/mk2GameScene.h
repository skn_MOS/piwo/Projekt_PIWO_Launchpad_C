#pragma once

#include "SuspendableThread.hpp"
#include "mk2Scene.h"
#include "util/utils.h"

class Mk2;
struct mk2Button;

#include <chrono>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <thread>

class mk2GameScene final : public mk2Scene
{
public:
	using ClockType = std::chrono::high_resolution_clock;
	using TimePoint = ClockType::time_point;
	using TimeType = std::chrono::milliseconds;

	static constexpr auto maxWaitTimeMS = 8192;
	static constexpr auto maxWaitTime = TimeType(maxWaitTimeMS);

	mk2GameScene(uint32_t w, uint32_t h, Mk2* parent)
	  : mk2Scene(w, h, parent)
	  , state(GameState::stop)
	{}

	~mk2GameScene() { this->cancel(); }

	virtual void handleButtonPressed(const mk2Button& button) override;
	virtual void enter() override;
	virtual void leave() override;

	virtual std::string_view getStatus() override
	{
		return "GameScene: all good";
	}

	virtual std::string_view getName() const override
	{
		return "GameScene";
	}

private:
	void cancel();
	void restart(std::chrono::milliseconds time);
	void enableClick();
	void handleClick();
	void wait();

	enum class GameState
	{
		stop,
		focus,
		click,
	};

	TimePoint timeStarted, timeEnded;
	GameState state;
	std::mutex mtx;
	std::condition_variable cv{};
	TimeType sleeptime;
	SuspendableThread waitThread = [this]() { wait(); };
};

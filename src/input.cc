// Callback procedures that are passed to midi driver
#include "IInputHandler.h"
#include "launchpad_core.hpp"
#include "mk2types.h"

static mk2Button
getButtonPressed(const uint32_t& midiData)
{
	mk2Button ret;
	uint32_t button = midiData & 0x0000ff00;
	uint32_t pressed = midiData & 0x00ff0000;

	pressed = pressed >> 16;
	button = button >> 8;
	button -= 0xb;
	uint32_t y = button / 10;
	uint32_t x = button % 10;

	ret.XY = static_cast<byte>((x << 4) | y);
	ret.state = static_cast<byte>(pressed);

	return ret;
}

static bool
checkButtonSpecial(uint32_t button)
{
	return (button & 0xff) == 0xf0;
}

static constexpr mk2Button buttonSpecial{ 0x00, 0x7f };

#ifdef _WIN32
#include <windows.h>
void
MidiInputProc(HMIDIIN hMidiIn __attribute__((unused)),
              uint32_t wMsg,
              DWORD_PTR dwInstance,
              DWORD_PTR dwParam1,
              DWORD_PTR dwParam2 __attribute__((unused)))
{
	debug_print("INPUT MIDI CALLBACK\n");

	auto buttonHandler = (ButtonActionHandler*)dwInstance;

	uint32_t buttonRaw = static_cast<uint32_t>(dwParam1);

	// TODO learn how to make proper binary data parsers
	// perhaps some finite state automaton?
	// or regex ;O
	mk2Button button = checkButtonSpecial(buttonRaw)
	                     ? buttonSpecial
	                     : getButtonPressed(buttonRaw);

	switch (wMsg) {
		case MIM_OPEN:
			debug_printerr("wMsg=MIM_OPEN\n");
			break;
		case MIM_CLOSE:
			debug_printerr("wMsg=MIM_CLOSE\n");
			break;
		case MIM_DATA:
			buttonHandler->handleButtonPressed(button);
			break;
		case MIM_LONGDATA:
			debug_printerr("wMsg=MIM_LONGDATA\n");
			break;
		case MIM_ERROR:
			debug_printerr("wMsg=MIM_ERROR\n");
			break;
		case MIM_LONGERROR:
			debug_printerr("wMsg=MIM_LONGERROR\n");
			break;
		case MIM_MOREDATA:
			debug_printerr("wMsg=MIM_MOREDATA\n");
			break;
		default:
			debug_printerr("wMsg = unknown\n");
			break;
	}
	return;
}

void
MidiOutputProc(HMIDIOUT hmo __attribute__((unused)),
               uint32_t wMsg,
               DWORD_PTR dwInstance,
               DWORD_PTR dwParam1 __attribute__((unused)),
               DWORD_PTR dwParam2 __attribute__((unused)))
{
	auto launchpadCore = (Mk2Core*)dwInstance;
	switch (wMsg) {
		case MOM_OPEN:
			break;
		case MOM_CLOSE:
			break;
		case MOM_DONE:
			midiOutUnprepareHeader(
			  launchpadCore->hMidiDeviceOut,
			  &launchpadCore->midiOutPacket,
			  sizeof(launchpadCore->midiOutPacket));
			break;
		case MOM_POSITIONCB:
			break;
	}
	return;
}
#elif __linux__
void
MidiInputReader(Mk2Core& master)
{
	uint32_t buf = 0;
	size_t bytes_read = 0;
	char dummy[8];

	while (bytes_read != 3) {
		auto ret =
		  read(master.deviceDescriptorRead, &buf, 3 - bytes_read);
		if (ret <= -1)
			return;
		bytes_read += static_cast<size_t>(ret);
		debug_print("button\n");
	}

	buf &= 0x00ffffff;

	// TODO learn how to make proper binary data parsers
	// perhaps some finite state automaton?
	// or regex ;O
	mk2Button button;

	if (checkButtonSpecial(buf)) {
		button = buttonSpecial;
		read(master.deviceDescriptorRead, &dummy, 5);
	} else {
		button = getButtonPressed(buf);
	}

	master.button_handler->handleButtonPressed(button);
}
#endif

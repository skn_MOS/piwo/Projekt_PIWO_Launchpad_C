#pragma once
#include <vector>

#include "clientry.hpp"
#include "util/utils.h"

// enum class CliStatus
//{
//	ok = 0,
//	quit,
//	err,
//};

class cliiface
{
public:
	using entrytable_t = std::vector<CLIEntry>;

private:
	entrytable_t entrytable;

public:
	template<typename EntrySequenceType>
	cliiface(EntrySequenceType&& entryseq)
	  : entrytable(std::forward<EntrySequenceType>(entryseq))
	{}

	cliiface() = default;
	~cliiface() = default;

	cliiface(const cliiface& other) = default;
	cliiface(cliiface&& other) = default;

	cliiface& operator=(const cliiface& other) = default;
	cliiface& operator=(cliiface&& other) = default;

	// Print cli menu
	void printCLI() const noexcept;

	// Handles user input from stdin.
	void cli() const noexcept;

	// Handle string command.
	CliResult cli(std::string_view command,
	              std::string_view args) const noexcept;

	void _always_inline add(const CLIEntry& entry)
	{
		this->entrytable.push_back(entry);
	}

	void _always_inline add(CLIEntry&& entry)
	{
		this->entrytable.push_back(std::move(entry));
	}
};

#ifndef _MK2MAINSCENE_H_
#define _MK2MAINSCENE_H_

#include "RenderEngineCore.h"
#include "SuspendableThread.hpp"
#include "lg_handlers.h"
#include "lg_iproto.h"
#include "lg_oproto.h"
#include "mk2ColorpickScene.h"
#include "mk2Scene.h"
#include "util/utils.h"

#ifdef _WIN32
#include <windows.h>
#elif __linux__
#endif

#include <condition_variable>
#include <cstdint>
#include <cstdlib>
#include <list>
#include <memory>
#include <mutex>
#include <optional>
#include <queue>
#include <thread>
#include <utility>
#include <vector>

#include <mipc/pipe_conn.h>
#include <mipc/pipe_listener.h>

#define ANIMATIONSCOUNT 5

// Packets that can be received from gui
// TODO change to enum class
enum resizePipeState
{
	rejected,
	confirmed,
	voidstate
};

class Mk2;
struct mk2Button;

// Class for main scene that is used to color buttons ( windows )
// Erase single or all buttons ( windows )
// Inherits from virtual mk2Scene and overwrites appropriately
// handleButtonPressed method to deliver behavior for this
// particular scene
class mk2MainScene final
  : public mk2Scene
  , private RenderEngineCore<mk2MainScene>
{
public:
	using RenderCoreType = RenderEngineCore<mk2MainScene>;
	using event_t = std::condition_variable;

	static constexpr std::string_view renderShmName = "lpd_display_buffer";

private:
	constexpr RenderEngineCore& getRenderCore()
	{
		return *static_cast<RenderCoreType*>(this);
	}

private:
	// Locks requestQ
	std::mutex requestQmutex;

	// Pipe used for communication with player
	mutable mipc::pipe::listener_t masterOutPipeListener;
	mutable mipc::pipe::conn_t masterOutPipe;

	// Handle to a thread that reads packets form pipe
	SuspendableThread pipeHandler;

	// Loaded animations ready to be rendered
	// Binded to buttons from 0x85 to 0x81
	piwo::animation animations[ANIMATIONSCOUNT];

	// Holds index in table of animations of animation
	// that is currently selected and will be rendered if any button pushed
	uint32_t currentAnim;
	static constexpr auto invalidAnim =
	  std::numeric_limits<decltype(currentAnim)>::max();

	// Holds currently picked color
	// mk2ColorpickScene will return here
	// picked color from its scene
	// MOST SIGNIFICANT BYTE IS RESERVED !
	//	 it is used to determine state of eraser
	uint32_t currentColor;

	// holds info whether rendercore should be resumed when
	// this scene is restored.
	bool resumeRenderOnRestore;

	// True if animation mode is turned on
	bool animBinded;

	// Used to restore colors of functional buttons
	void restoreFunctionButtons();

	// Helper function for initPipeForOutData
	// Returns same values as funtions that uses it ie initPipeForOutData
	// except fot -2 value
	// CAUTION: THIS METHOD IS NOT SAFE IN MOST CASES -- YOU PROBABLY WANT
	// TO USE initPipeForOutData
	int32_t createPipeForOutDataForced(const char* pipename);
	int32_t createPipeForOutData(const char* pipename);

	// If animation is loaded ( binded ) to given button sets its color to
	// be unselected but if animation isnt binded leaves it blacked out IT
	// DOESNT CHANGE STATE OF CURRENTLY SELECTED SCENE IT ONLY AFFECTS
	// BUTTONS COLOR
	void unselectAllAnimsG();

	// unselects all animations also in graphical mode
	void unselectAllAnims();

	// Handles communication that is needed when changing size of frame
	// Returns true if resize has been confirmed, false otherwise.
	bool handleGUIResize(uint32_t width, uint32_t height);

	// Waits for confirmation of resize packet. Returns true if confirmation
	// has been received, false if not or timedout
	bool waitForResizeConfirm();

public:
	// Calls parent constructor with same arguments as passed to it
	mk2MainScene(Mk2* lp, uint32_t, uint32_t);

	// Deallocs main memory and closes renderLoop thread
	~mk2MainScene();

	// For this particular class handleButtonPressed will
	// color each clicked button on currently selected color
	// If top right button is pressed it will change to scene
	// where you select new color for painting
	// Eraser should be just below color picking
	// Clear whole pad functionality should be on most bottom
	// right functional button.
	// All changes to buttons need to be performed via connection
	// that is already established, thats why main launchpad is
	// passed in argument
	virtual void handleButtonPressed(const mk2Button& button) override;

	// See mk2Scene::enter
	virtual void enter() override;

	// See mk2Scene::leave
	virtual void leave() override;

	// Used to initialize pipe for sending data out about key pushed on
	// launchpad If pipe is already opened, closes pipe and open it again.
	// Returns 0 if successfully created pipename
	// Returns -1 if cannot create pipe
	// Returns -2 if pipe is already created
	// This fucntion is not overlapped. It returns when master connect to
	// pipe.
	int32_t initPipeForOutData(const char* pipename, int32_t forced = 0);

	// Used to restore colors of buttons on launchpad
	// and colors of functional buttons
	void restoreGrid();

	// Sets all colors in local memory table to black
	void zeroMemory();

	// Restores colors on animation buttons
	void restoreAnimState();

	// Deallocs and reallocs new main memory in size described by
	// passed parameters
	virtual int reallocMainMem(uint32_t width, uint32_t height) override;

	// Prints state of mainTable to console
	// for debug purpuses
	void showTable();

	// Creates RenderObject based on currently selected animation
	std::optional<RenderObject> createRenderObject(uint32_t xoff,
	                                               uint32_t yoff,
	                                               uint32_t animID);

	// Selects animation -- unselects previous and selects one described
	// from argument passed. Returns true if animation has been successfully
	// binded or unbinded
	bool selectAnim(uint32_t animation);

	// If pipe is valid, sends bytes.
	// Returns bytes successfully send via pipe
	int32_t sendPipeDataWCheck(const void* data,
	                           const uint32_t datasize) const;

	void closePipe();

	// callback for rendercore to call when frame is ready
	void renderCallback(FrameBase* frame) const;

	// ColorpickScene is allowed to change colors
	friend void mk2ColorpickScene::changeMainsCurrentColor(
	  uint32_t c,
	  mk2MainScene* ms);
	friend void mk2ColorpickScene::changeMainsCurrentColor(uint32_t c,
	                                                       mk2Scene* ms);

	friend uint32_t lgp_setXY(mk2MainScene* mk2,
	                          uint32_t x,
	                          uint32_t y,
	                          uint32_t color);
	friend uint32_t lgp_animframe(mk2MainScene* mk2,
	                              uint32_t width,
	                              uint32_t height,
	                              void* data);
	friend uint32_t lgp_clearall(mk2MainScene* mk2);
	friend uint32_t lgp_resize(mk2MainScene* mk2,
	                           uint32_t width,
	                           uint32_t height);
	friend void readPipeThread(mk2MainScene* mainSceneptr);

	friend void lg_ip_nop_handler(mk2MainScene* ms);
	friend void lg_ip_resize_handler(mk2MainScene*,
	                                 lg_local_buf_t,
	                                 uint32_t);
	friend void lg_ip_accept_handler(mk2MainScene* ms);
	friend void lg_ip_reject_handler(mk2MainScene* ms);
	friend void lg_ip_end_handler(mk2MainScene* ms);

	virtual std::string_view getStatus() override
	{
		return "MainScene: all good";
	}

	virtual std::string_view getName() const override
	{
		return "MainScene";
	}

	auto getCurrentColor() const { return this->currentColor; }
	bool shmem_valid() const { return this->canvas.shmem_valid(); }
	std::string_view shmem_name() const
	{
		return this->canvas.shmem_name();
	}
};

#endif

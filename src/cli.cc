#include "cli.hpp"

#include "util/utils.h"
#include "util/debugprint.h"

#include <cassert>
#include <fmt/format.h>
#include <iostream>

void
cliiface::printCLI() const noexcept
{
	log_stdout("~~~ CLI ~~~\n");

	if (UNLIKELY(this->entrytable.size() == 0)) {
		log_stdout("cli menu is empty\n");
		return;
	}

	size_t i = 1;
	for (auto& e : this->entrytable) {
		log_stdout("{}: {} -- {}\n", i, e.getName(), e.getDesc());
		++i;
	}
}

#define CHECK_STREAM(stream)                                                   \
	if (stream.fail()) {                                                   \
		puts("Bad args");                                              \
		stream.ignore(1337, '\n');                                     \
		stream.clear();                                                \
		continue;                                                      \
	}

struct CliCommandDecomposed
{
	std::string_view command, args;
};

static std::string_view
makeStringView(std::string::const_iterator l, std::string::const_iterator r)
{
	assert(l <= r);
	return std::string_view(&(*l), static_cast<size_t>(r - l));
}

static CliCommandDecomposed
decomposeCliCommand(const std::string& input)
{
	CliCommandDecomposed c;

	auto begin = input.begin();
	auto end = std::find(std::begin(input), std::end(input), ' ');
	c.command = makeStringView(begin, end);

	if (end == std::end(input))
		return c;

	while (*(++end) == ' ')
		;

	begin = end;
	end = std::find(begin, std::end(input), '\0');
	c.args = makeStringView(begin, end);

	return c;
};

void
cliiface::cli() const noexcept
{
	std::string input;

	while (1) {
	while_begin:
		printCLI();
		std::getline(std::cin, input);

		if (input == "quit")
			return;

		const auto& [command, args] = decomposeCliCommand(input);

		for (auto& e : this->entrytable) {
			if (e.cmpcmd(command)) {
				CliResult result = e.callHandler(args);
				std::visit(
				  overloaded{
				    []([[maybe_unused]] CliResultOk& ok) {
					    log_stdout("Ok\n");
				    },
				    [](CliResultError& err) {
					    log_stdout("{}\n", err.error);
				    },
				  },
				  result);
				goto while_begin;
			}
		}

		log_stdout("\n\nError: Unknown command!\n\n");
	}
}

CliResult
cliiface::cli(std::string_view command, std::string_view args) const noexcept
{
	for (auto& e : this->entrytable)
		if (e.cmpcmd(command))
			return e.callHandler(args);
	return CliResultError("Error: command not found");
}

#undef CHECK_STREAM

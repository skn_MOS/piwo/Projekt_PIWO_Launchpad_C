// ==== ++++ ====
// INPUT LAUNCHPAD <- GUI PROTOCOL LIBRARY
// version 3.0
// This is implementation of launchpad to GUI PROTOCOL
// documented in SKN MOS DOCS
// ==== ++++ ====

#pragma once
#include <cstdint>

using lg_iproto_t = uint32_t;

constexpr lg_iproto_t LG_IP_NOP = 0x00;
constexpr lg_iproto_t LG_IP_RESIZE = 0x01;
constexpr lg_iproto_t LG_IP_ACCEPT = 0x02;
constexpr lg_iproto_t LG_IP_REJECT = 0x03;
constexpr lg_iproto_t LG_IP_END = 0x04;

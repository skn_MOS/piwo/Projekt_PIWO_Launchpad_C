#pragma once

#include "mk2Scene.h"
#include <string_view>

class Mk2;
struct mk2Button;

class mk2PrintScene final : public mk2Scene
{
public:
	mk2PrintScene(uint32_t w, uint32_t h, Mk2* parent)
	  : mk2Scene(w, h, parent)
	{}

	virtual void handleButtonPressed(const mk2Button& button) override;
	virtual void enter() override;
	virtual void leave() override;

	void restoreScene();

	void print(std::string_view s) const;

	virtual std::string_view getName() const override
	{
		return "PrintScene";
	}
};

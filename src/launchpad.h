#ifndef LAUNCHPAD_H_
#define LAUNCHPAD_H_
#include <array>
#include <memory>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string_view>

#ifdef _WIN32
#include <windows.h>
#elif __linux__
#include <fcntl.h>
#include <unistd.h>

using DWORD = uint32_t;
using LONG = uint32_t;
#endif

#include "util/commondef.h"
#include "util/debugprint.h"
#include "util/spinlock.h"

#include "launchpad_core.hpp"
#include "lpPacketTemplates.h"

#include "mk2ColorpickScene.h"
#include "mk2GameScene.h"
#include "mk2MainScene.h"
#include "mk2PrintScene.h"
#include "mk2Scene.h"

#include "IInputHandler.h"

#include <mipc/pipe_listener.h>

#if defined(LPD_WITH_NCURSES)
#include <ncurses.h>
#endif

// Name of pipe that is used for lp->gui communication
constexpr const char* MASTERLGPIPE = "masterlgpipe";

class Mk2
  : public Mk2Core
  , public ButtonActionHandler
{
public:
	template<typename T, T defaultRet>
	class Stack
	{
	public:
		using Element = T;
		using Container = std::vector<Element>;

		template<typename U>
		void push(U&& e)
		{
			this->stack.emplace_back(std::forward<U>(e));
		}

		Element pop()
		{
			if (this->stack.size() == 0)
				return defaultRet;

			auto ret = this->stack.back();
			this->stack.pop_back();
			return ret;
		}

		auto begin() const { return stack.begin(); }
		auto end() const { return stack.end(); }

	private:
		Container stack;
	};

	using ArgStackElement = intptr_t;
	Stack<ArgStackElement, 0> argStack;

private:
	Stack<SCENEID, SCENEID::DEFAULT> sceneStack;

	// Mk2 works just fine with these two,
	// so it will remain const for now
	const DWORD midiPortIn = 0;
	const DWORD midiPortOut = 1;

	// Holds value of currently active scene
	SCENEID currentScene;

#if defined(LPD_OMIT_REDUNDANT_WRITES)
	void flushFrameToLaunchpadSkipRedundant(const FrameBase &frame);

	// Holds state of colors that are visible on the lanuchpad.
	// This is used to skip redundant ones.
	Frame mirrorFrame;

	// Used for forcing redrawing of pixels even if they seem to not have
	// changed after checking in mirrorFrame.
	bool forceNextRedraw = false;
#endif

	// Main scene to colorize buttons on click
	std::unique_ptr<mk2MainScene> mainScene;

	// Scene for changing current color of launchpad
	std::unique_ptr<mk2ColorpickScene> colorpickScene;

	// Scene for displaying scrolling text
	std::unique_ptr<mk2PrintScene> printScene;

	// Scene for for playing various games
	std::unique_ptr<mk2GameScene> gameScene;

	// Table of all scenes
	static constexpr uint32_t sceneCount =
	  underlay_cast(SCENEID::TERMINATORSCENEID);

	// Make sure NOT to make scenes array too big. If necessary increment
	// counter in the static_assert. It is checked to make sure someone does
	// not give huge number to one of the scenes. We hope to catch this at
	// compile time.
	static_assert(sceneCount == 4);
	mk2Scene* scenes[sceneCount];

	// prints all devices in a simple list
	void printMidiDevices(const uint32_t devOutCnt,
	                      const uint32_t devInCnt) const;

	// prints midi devices choosed with labels
	// first argument is input device id
	// second is output device id
	void printMidiDevicesByID(const uint32_t indevid,
	                          const uint32_t outdevid) const;

	// Checks wheter given button XY contains in main grid of lp
	bool isGrid(const mk2Button& btn) const;

	// Prints big ascii message "sknmos software"
	void printSKNMOS();

	// Flushes main grid data to Mk2 device using blocking IO.
	void flushFrameToLaunchpadAll(const FrameBase& frame) const;

private:
#if defined(LPD_WITH_NCURSES)
	struct CliWindow
	{
		WINDOW* wnd;
		std::string title;
		int currentLine = 1;

		CliWindow(int h, int w, int y, int x, std::string title = "")
		  : wnd(newwin(h, w, y, x))
		  , title(std::move(title))
		{}

		CliWindow(WINDOW* other)
		  : wnd(other)
		{}

		~CliWindow() { delwin(wnd); }

		template<typename T, typename... Args>
		void drawLine(T&& fmtstr, Args&&... args)
		{
			// FIXME: Fix this garbage code - make the windows
			// scrollable and make it autoscroll on output

			std::string s = fmt::format(fmt::runtime(fmtstr), std::forward<Args>(args)...);

			if (s.empty())
				return;

			if (s.back() == '\n')
				s.pop_back();

			wprintw(
			  wnd,
			  s.c_str());

			++wnd->_cury;
			wnd->_curx = 1;

			// If we overflowed, try printing again
			if (wnd->_cury >= wnd->_maxy) {
				wnd->_cury = 1;
				wclear(wnd);

				wprintw(
				  wnd,
				  s.c_str());

				++wnd->_cury;
				wnd->_curx = 1;

				// If overflow while trying again, just let it be..
				if (wnd->_cury >= wnd->_maxy)
					wnd->_cury = wnd->_maxy;
			}
		}

		template<typename T, typename... Args>
		void draw(T&& fmtstr, Args&&... args)
		{
			wprintw(
			  wnd,
			  fmt::format(fmt::runtime(fmtstr), std::forward<Args>(args)...).c_str());
		}

		Position<int> getPos()
		{
			return { wnd->_begx + wnd->_curx,
				 wnd->_begy + wnd->_cury };
		}

		void skipLine()
		{
			++wnd->_cury;
			wnd->_curx = 1;
		}

		void refresh() { wrefresh(wnd); }

		void drawBorder()
		{
			box(wnd, 0, 0);
			int midx = (wnd->_maxx / 2) -
			           (static_cast<int>(title.size()) / 2);
			mvwprintw(wnd, wnd->_begy - 1, midx, title.c_str());
		}

		void reset()
		{
			werase(wnd);
			wnd->_cury = 1;
			wnd->_curx = 1;
		}
	};

	struct CliDrawContext
	{

#define WINDOW_HEADER_HEIGHT 1
#define COMMAND_WINDOW_HEIGHT 4
#define COMMAND_WINDOW_WIDTH maxx
#define COMMAND_WINDOW_Y (maxy - COMMAND_WINDOW_HEIGHT)
#define COMMAND_WINDOW_X 0
#define INFO_WINDOW_HEIGHT (maxy - WINDOW_HEADER_HEIGHT - COMMAND_WINDOW_HEIGHT)
#define INFO_WINDOW_WIDTH (maxx / 2)
#define INFO_WINDOW_Y WINDOW_HEADER_HEIGHT
#define INFO_WINDOW_X 0
#define LOG_WINDOW_HEIGHT INFO_WINDOW_HEIGHT
#define LOG_WINDOW_WIDTH (maxx / 2)
#define LOG_WINDOW_Y WINDOW_HEADER_HEIGHT
#define LOG_WINDOW_X (maxx / 2)

		int maxy, maxx;
		static constexpr std::string_view header = "LPD CLIv2";
		CliWindow infoWindow, commandWindow, logWindow;

		static CliDrawContext CreateCliDrawContext()
		{
			int maxy, maxx;
			getmaxyx(stdscr, maxy, maxx);
			return CliDrawContext(maxy, maxx);
		}

		CliDrawContext(int maxy, int maxx)
		  : maxy(maxy)
		  , maxx(maxx)
		  , infoWindow(INFO_WINDOW_HEIGHT,
		               INFO_WINDOW_WIDTH,
		               INFO_WINDOW_Y,
		               INFO_WINDOW_X,
		               "Info")
		  , commandWindow(COMMAND_WINDOW_HEIGHT,
		                  COMMAND_WINDOW_WIDTH,
		                  COMMAND_WINDOW_Y,
		                  COMMAND_WINDOW_X)
		  , logWindow(LOG_WINDOW_HEIGHT,
		              LOG_WINDOW_WIDTH,
		              LOG_WINDOW_Y,
		              LOG_WINDOW_X,
		              "Log")
		{
			assert(infoWindow.wnd &&
			       "info window creation failed.");
			assert(commandWindow.wnd &&
			       "command window creation failed.");
			assert(logWindow.wnd && "log window creation failed.");
		}

		void drawHeader()
		{
			getmaxyx(stdscr, maxy, maxx);
			mvprintw(0,
			         (maxx / 2) -
			           (static_cast<int>(header.size()) / 2),
			         header.data());
		}

#undef WINDOW_HEADER_HEIGHT
#undef COMMAND_WINDOW_HEIGHT
#undef COMMAND_WINDOW_WIDTH
#undef COMMAND_WINDOW_Y
#undef COMMAND_WINDOW_X
#undef INFO_WINDOW_HEIGHT
#undef INFO_WINDOW_WIDTH
#undef INFO_WINDOW_Y
#undef INFO_WINDOW_X

	} mutable drawContext = CliDrawContext::CreateCliDrawContext();

	// Draw cli status.
	// @param errmsg Last error message of the last command.
	void drawStatus(std::string_view errmsg) const;
#endif

public:
	// Calling launchpad core ctor that initializes connection with
	// mk2 launchpad device and sets up callbacks, creates scenes
	Mk2();

	~Mk2() = default;

	// Is used to light up a given by XY coordinates
	// led on launchpad on a given color
	// color is 3byte uint shifted appropriately
	// |00|RR|GG|BB|
	bool setLedXY(const uint32_t x, const uint32_t y, const uint32_t color);

	bool pulseLedXY(const uint32_t x, const uint32_t y, Mk2Color c);

	// Same as setLedXY but checks first if
	// XY of button that will be changed is in
	// main grid of buttons ( is not functional button )
	bool setGridLedXY(const uint32_t x,
	                  const uint32_t y,
	                  const uint32_t color);

	// Used to set single button to given color
	// only difference from setLedXY is that coordinates
	// are in form returned by launchpad and color is in range
	// 0x00 - 0xf7
	bool setLedB(const byte XY, const uint32_t color);

	// Returns value of color for requested led
	int64_t getLedXY(const uint32_t x, const uint32_t y);

	// Sets all leds to given color
	// color is 3byte uint shifted appropriately
	// |00|RR|GG|BB|
	bool setLedAll(Mk2Color color);

	bool setString(std::basic_string_view<byte> str,
	               Mk2Color c,
	               Mk2Speed s) const;

	bool setString(std::string_view data,
	                      Mk2Color c,
	                      Mk2Speed s) const
	{
		return setString(
		  { reinterpret_cast<const byte*>(data.data()), data.size() },
		  c,
		  s);
	}

	// Clears all buttons of launchpad
	// It calls setLedAll(0x00)
	void clearPad()
	{
#if defined(LPD_OMIT_REDUNDANT_WRITES)
		this->forceNextRedraw = true;
#endif
		this->setLedAll(Mk2Color::black);
	}

	// Flushses 8x8 grid from current position to launchpad
	void flushFrameToLaunchpad(const FrameBase& frame);

	auto getCurrentScene() const
	{
		return this->currentScene;
	}

	// Returns pointer const pointer to given scene
	auto getScene(SCENEID sceneID)
	{
		return this->scenes[static_cast<uint32_t>(sceneID)];
	}

	// Prints info about supported file formats
	void printSupportedFormats() const
	{
		printf("[LAUNCHPAD] Supported formats: PIWO7(PIWO_7_FILE).\n");
	}

	// Handles pressed button appropriately to currently set scene
	virtual void handleButtonPressed(
	  const struct mk2Button& button) override;

	// Handles switching of scene.
	// Sets currentScene and calls restoreScene of new scene
	void switchScene(SCENEID scene);

	// prints saved state of buttons
	// just for debug
	void showTable() const;

	//	Wrapper on piwo7Scene.handlePiwo7()
	void handlePiwo7(const char* name);

	// Runs launchpad main cli which allows to select given scenes cli
	void cli();

};

// Decodes launchpad data into structure
// that holds information about lastly pressed
// button and press / release information
// XY coordianes are packed into 1 byte
// most significant nibble is X coordinate and
// the least significant one is Y coordinate
byte
getButtonPressed(const uint32_t& midiData, mk2Button& mk2button);

#endif // LAUNCHPAD_H_

#pragma once
#include <algorithm>
#include <string>
#include <string_view>
#include <variant>
#include <functional>

#include "util/utils.h"

struct CliResultOk
{};

struct CliResultError
{
	CliResultError() = default;

	template<typename... Args>
	CliResultError(Args&&... args)
	  : error(std::forward<Args>(args)...)
	{}

	std::string error;
};

using CliResult = std::variant<CliResultOk, CliResultError>;

class CLIEntry
{
public:
	using handler_t = std::function<CliResult(std::string_view)>;

private:
	std::string name;
	std::string desc;
	handler_t handler;

public:
	template<typename StringType1, typename StringType2>
	CLIEntry(StringType1&& name,
	         StringType2&& desc,
	         handler_t handler) noexcept
	  : name(std::forward<StringType1>(name))
	  , desc(std::forward<StringType2>(desc))
	  , handler(handler)
	{}

	CLIEntry(const CLIEntry& other) = default;
	CLIEntry(CLIEntry&& other) = default;

	CLIEntry& operator=(const CLIEntry& other) = default;
	CLIEntry& operator=(CLIEntry&& other) = default;

	~CLIEntry() = default;

	[[nodiscard]] const auto& getName() const noexcept
	{
		return this->name;
	}

	[[nodiscard]] const auto& getDesc() const noexcept
	{
		return this->desc;
	}

	[[nodiscard]] bool cmpcmd(
	  std::string_view command) const noexcept
	{
		return this->name == command;
	}

	CliResult _always_inline
	callHandler(std::string_view args) const noexcept
	{
		return this->handler(args);
	}
};

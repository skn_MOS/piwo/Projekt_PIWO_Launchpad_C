#include "mk2GameScene.h"
#include "launchpad.h"
#include "mk2PrintScene.h"

#include <chrono>

using namespace std::chrono_literals;

static auto
currentSysTime()
{
	return std::chrono::high_resolution_clock::now();
}

void
mk2GameScene::handleButtonPressed(const mk2Button& button)
{
	auto lp = this->parent;

	debug_print("Got button: {}x{} on board: {}x{}\n",
	            static_cast<uint32_t>(button.XY >> 4),
	            static_cast<uint32_t>(button.XY & 0x0f),
	            this->mainTable.getWidth(),
	            this->mainTable.getHeight())

	  switch (button.XY)
	{
		case 0x87:
			lp->switchScene(SCENEID::GAMEPICKSCENEID);
			break;
		default:
			this->handleClick();
	}
}

void
mk2GameScene::enter()
{
	SCOPED(std::unique_lock lck(this->mtx))
	{
		srand(static_cast<unsigned int>(time(0)));
		this->parent->setLedAll(Mk2Color::red);
		this->sleeptime =
		  TimeType(rand() % mk2GameScene::maxWaitTimeMS);
		this->state = GameState::focus;
		this->waitThread.resume();
	}
}

void
mk2GameScene::leave()
{
	SCOPED(std::unique_lock lck(this->mtx))
	{
		this->waitThread.stop();
		this->cv.notify_one();
		this->state = GameState::stop;
	}
}

void
mk2GameScene::cancel()
{
	this->waitThread.stop();

	std::unique_lock lck(mtx);
	this->cv.notify_one();
}

void
mk2GameScene::restart(std::chrono::milliseconds time)
{
	this->cancel();
	this->sleeptime = time;
	this->waitThread.resumeAndWait();
}

void
mk2GameScene::enableClick()
{
	if (this->state != GameState::focus)
		return;

	this->parent->setLedAll(Mk2Color::green);
	this->timeStarted = currentSysTime();
	this->state = GameState::click;
	this->waitThread.stop();
}

void
mk2GameScene::handleClick()
{
	using namespace std::chrono_literals;

	GameState state;

	SCOPED(std::unique_lock lck(this->mtx))
	{
		state = this->state;
		this->state = GameState::stop;
	}

	auto printScene = static_cast<mk2PrintScene*>(
	  this->parent->getScene(SCENEID::PRINTSCENEID));

	if (state != GameState::click) {
		this->state = GameState::stop;
		this->parent->setLedAll(Mk2Color::blue);
		printScene->print("Too early");
		return;
	}

	this->timeEnded = currentSysTime();

	auto dtime = this->timeEnded - this->timeStarted;
	auto dtimems =
	  std::chrono::duration_cast<std::chrono::milliseconds>(dtime).count();

	log_stdout("Time: {}\n", dtimems);

	printScene->print(fmt::format("{}ms\n", dtimems));
}

void
mk2GameScene::wait()
{
	std::cv_status status;

	std::unique_lock lck(mtx);
	status = this->cv.wait_for(lck, this->sleeptime);

	if (status == std::cv_status::timeout)
		this->enableClick();
}

#pragma once

#include <atomic>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <thread>

#include "util/debugprint.h"
#include "util/utils.h"

#ifdef __WIN32
#include <processthreadsapi.h>
#elif __linux__
#include <signal.h>
#endif

class Synchronizable
{
private:
	using event_t = std::condition_variable;

	// Mutex for changing sleep / wake state
	std::mutex eventMutex;

	// Event that tells synchronized object to go to sleep
	event_t wakeUpEvent;

	// Event that tells user of synchronizer that object is sleeping
	event_t goingSleepEvent;

	// Event that tells user of synchronizer that object is waking up
	event_t wakingUpEvent;

	std::atomic_bool stopRequested{ true };
	std::atomic_bool stopDone{ false };
	std::atomic_bool alive{ true };

public:
	void stop() { this->stopRequested = true; }

	void stopAndWait()
	{
		// Changing state, aquire mutex
		std::unique_lock<std::mutex> lck(this->eventMutex);

		// Mark stop request
		this->stop();

		// Wait for thread to go sleep
		this->goingSleepEvent.wait(
		  lck, [this]() -> bool { return this->stopDone; });
	}

	void resume()
	{
		this->stopRequested = false;
		this->wakeUpEvent.notify_all();
	}

	void resumeAndWait()
	{
		std::unique_lock<std::mutex> lck(this->eventMutex);

		this->resume();

		this->wakingUpEvent.wait(lck,
		                         [this] { return !this->stopDone; });
	}

	bool isStopped() { return this->stopDone; }

	void wait()
	{
		if (this->stopRequested) {
			// Changing state, aquire mutex
			std::unique_lock<std::mutex> lck(this->eventMutex);

			// Mark sleep state
			this->stopDone = true;

			// Notify all that is waiting for this thread to sleep
			this->goingSleepEvent.notify_all();

			// Wait until someone wakes this thread, wait will also
			// release event mutex
			this->wakeUpEvent.wait(
			  lck, [this] { return !this->stopRequested; });

			this->stopDone = false;
			this->wakingUpEvent.notify_all();
		}
	}

	bool isStopRequested() { return this->stopRequested; }

	void kill()
	{
		this->alive = false;
		this->resume();
	}

	bool isAlive() { return this->alive; }
};

class SuspendableThread : public Synchronizable
{
private:
	std::thread worker;

public:
	SuspendableThread() = default;

	template<class T, class... Args>
	SuspendableThread(T&& callable, Args&&... args)
	{
		this->worker = std::thread(
		  [this](T&& callable, Args&&... args) {
			  while (1) {
				  this->wait();

				  if (!this->isAlive())
					  break;

				  std::invoke(std::forward<T>(callable),
				              std::forward<Args>(args)...);
			  }
		  },
		  std::forward<T>(callable),
		  std::forward<Args>(args)...);
	}

	SuspendableThread& operator=(const SuspendableThread& thread) = delete;
	SuspendableThread& operator=(SuspendableThread&& thread) = delete;

	~SuspendableThread()
	{
		debug_printerr("Killing thread witbool h id {}\n", id);
		this->kill();
		if (this->worker.joinable())
			this->worker.join();
		debug_printerr("Thread with id {} killed\n", id);
	}

	void join() noexcept { worker.join(); }

	auto native_handle() noexcept
	{
		return this->worker.native_handle();
	}

	void _terminate() noexcept
	{
#ifdef __WIN32
		::TerminateThread(
		  pthread_gethandle(this->worker.native_handle()), 1);
#elif __linux__
		::pthread_kill(this->worker.native_handle(), SIGUSR1);
#endif
		this->worker.detach();
	}
};

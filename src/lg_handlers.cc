#include "mk2MainScene.h"

void
lg_ip_nop_handler([[maybe_unused]] mk2MainScene* ms)
{
	return;
}

void
lg_ip_resize_handler(mk2MainScene* ms, lg_local_buf_t buff, uint32_t read)
{
	constexpr auto total_packet_size =
	  lg_ip_resize_total_size<decltype(read)>;
	char* local_buf = reinterpret_cast<char*>(buff.data());

	assert(buff[0] == LG_IP_RESIZE);

	while (read < total_packet_size) {
		auto read_remain = total_packet_size - read;
		char* local_buf_curr = local_buf + read;

		int read_ = ms->masterOutPipe.read(local_buf_curr, read_remain);

		if (UNLIKELY(read_ <= 0)) {
			ms->closePipe();
			ms->pipeHandler.stop();
			return;
		}

		read += static_cast<uint32_t>(read_);
	}

	auto new_width = buff[1];
	auto new_height = buff[2];

	ms->handleGUIResize(new_width, new_height);
}

void
lg_ip_accept_handler([[maybe_unused]] mk2MainScene* ms)
{}

void
lg_ip_reject_handler([[maybe_unused]] mk2MainScene* ms)
{}

void
lg_ip_end_handler(mk2MainScene* ms)
{
	constexpr bool auto_reconnect = true;

	ms->closePipe();
	ms->pipeHandler.stop();

	if (auto_reconnect) {
		debug_print("Attempting auto reconnect.\n");

		// FIXME(holz) THIS IS MY PERSONAL TOTAL FAILURE
		// this cancer code must be here because libmipc has
		// some weird bug in the windows implementation
		// that doesnt allow reaccepting on the same listener.
#ifdef __WIN32
		do {
			log_stdout("Creating listener pipe.\n");
			ms->masterOutPipeListener =
			  mipc::pipe::listener_t::create("lpd_fifo");
			std::this_thread::sleep_for(std::chrono::seconds(1));
		} while (!ms->masterOutPipeListener.is_valid());
#endif
		ms->masterOutPipe = ms->masterOutPipeListener.accept();
		if (!ms->masterOutPipe.is_valid()) {
			log_stdout("Accepting pipe failed.\n");
		}
		ms->pipeHandler.resume();
	}

	debug_print("Connected!\n");
}

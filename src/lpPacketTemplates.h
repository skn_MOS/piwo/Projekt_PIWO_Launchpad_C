#pragma once

#include <algorithm>
#include <array>
#include <cassert>
#include <memory>

#include "mk2types.h"
#include "util/commondef.h"
#include "util/debugprint.h"

enum class Mk2Color : byte;
enum class Mk2Speed : byte;

static constexpr int MK2_PACKET_HEADER_SIZE = 6;
static constexpr int MK2_PACKET_FOOTER_SIZE = 1;
static constexpr byte MK2_PACKET_TERMINATOR = 0xf7;

struct __attribute__((packed)) mk2PacketHeader
{
	const byte data[MK2_PACKET_HEADER_SIZE]{ 0xf0, 0x00, 0x20,
		                                 0x29, 0x02, 0x18 };
};

struct __attribute__((packed)) mk2PacketFooter
{
	const byte data[MK2_PACKET_FOOTER_SIZE]{ MK2_PACKET_TERMINATOR };
};

struct serialized
{
	byte* data;
	size_t size;
};

struct __attribute__((packed)) mk2PSetLedXYBulkHeader
{
	mk2PacketHeader hdr;
	const byte fun = 0x0b;
};

struct __attribute__((packed)) mk2PSetLedXYFrameBulkPayload
{
	byte xy, r, g, b;
};

/*
 * There is a way to send mk2PSetLedXY packets in bulk, 80 coordinates at once.
 * The structure of the packet is:
 *
 *     struct {
 *         mk2PacketHeader header;
 *         const byte fun = 0x0b;
 *
 *         // Array of 4 byte payloads for each pixel, can be repeated up to 80 times.
 *         struct { byte xy, r, g, b; }[1-80];
 *
 *         mk2PacketFooter footer;
 *     };
 *
 * Because of the variable length array, we build this structure at runtime
 * instead of using defined type.
 */
struct __attribute__((packed)) mk2PSetLedXY
{
	mk2PacketHeader hdr;
	const byte fun = 0x0b;
	byte xy, r, g, b;
	mk2PacketFooter ftr;

	serialized serialize()
	{
		serialized ret;
		ret.data = reinterpret_cast<byte*>(&this->hdr);
		ret.size = sizeof(*this);

		return ret;
	}
};

struct __attribute__((packed)) mk2PPulseLedXY
{
	mk2PacketHeader hdr;
	const byte fun[2] = { 0x28, 0x00 };
	byte xy, color;
	mk2PacketFooter ftr;

	serialized serialize()
	{
		serialized ret;
		ret.data = reinterpret_cast<byte*>(&this->hdr);
		ret.size = sizeof(*this);

		return ret;
	}
};

struct __attribute__((packed)) mk2PSetLedAll
{
	mk2PacketHeader hdr;
	const byte fun = 0x0e;
	byte color;
	mk2PacketFooter ftr;

	serialized serialize()
	{
		serialized ret;
		ret.data = reinterpret_cast<byte*>(this);
		ret.size = sizeof(*this);

		return ret;
	}
};

struct __attribute__((packed)) mk2PSetString
{
	static constexpr size_t maxStringLen = 127;

	size_t currentLength = 0;
	mk2PacketHeader hdr;
	const byte fun[1] = { 0x14 };
	byte color, loop, speed, data[maxStringLen];

	serialized serialize()
	{
		serialized ret;

		auto rawBegin = reinterpret_cast<byte*>(&this->hdr);
		auto rawEnd =
		  reinterpret_cast<byte*>(&this->data) + currentLength;
		*rawEnd = MK2_PACKET_TERMINATOR;

		assert(rawEnd - rawBegin >= 0);

		ret.data = rawBegin;
		ret.size = static_cast<size_t>(rawEnd - rawBegin + 1);

		return ret;
	}

	void clear() { this->currentLength = 0; }

	size_t append(std::basic_string_view<byte> data)
	{
		size_t freespace = this->maxStringLen - this->currentLength;
		size_t len = std::min(data.size(), freespace);

		auto insertTo = this->data + this->currentLength;
		auto insertFrom = std::cbegin(data);
		std::copy_n(insertFrom, len, insertTo);

		assert(len <= std::numeric_limits<byte>::max());
		this->currentLength += len;

		return len;
	}
};

struct __attribute__((packed)) mk2PInit
{
	mk2PacketHeader hdr;
	const byte fun[2] = { 0x22, 0x02 };
	mk2PacketFooter ftr;

	serialized serialize()
	{
		serialized ret;
		ret.data = reinterpret_cast<byte*>(this);
		ret.size = sizeof(*this);

		return ret;
	}
};

static constexpr size_t mk2PUpdatePacketBulkMaxSize =
  sizeof(mk2PSetLedXYBulkHeader) + LP_GRID_SIZE * sizeof(mk2PSetLedXYFrameBulkPayload) + sizeof(mk2PacketFooter);

using mk2PUpdatePacked = std::array<byte, mk2PUpdatePacketBulkMaxSize>;

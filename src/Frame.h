#pragma once

#include "mk2types.h"
#include "types.h"
#include "util/commondef.h"
#include "util/debugprint.h"
#include "util/time.h"
#include "util/utils.h"

#include <string_view>

#include <mipc/shmem.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

constexpr int DEFAULT_GRID_WIDTH = 8;
constexpr int DEFAULT_GRID_HEIGHT = 8;

// TODO(holz) allign to 32 bits
// Then use byte swap intrinsic when doing get3dim
constexpr int BYTES_PER_PIXEL = 3;

struct Size
{
	uint32_t width;
	uint32_t height;

	Size(uint32_t width, uint32_t height)
	  : width(width)
	  , height(height)
	{}

	Size() = default;

	auto getTotal() const
	{
		return this->width * this->height;
	}

	bool operator==(const Size& other)
	{
		return this->width == other.width &&
		       this->height == other.height;
	}

	bool operator!=(const Size& other) { return !(*this == other); }
};

class FrameBase;
inline int
copyFrame(FrameBase& frame1, const FrameBase& frame2);

class FrameBase
{
protected:
	byte* data;
	Size size;

	FrameBase()
	  : data(nullptr)
	{}

public:
	// Holds XY value of current position of launchpad frame
	// Used to control bigger screens than lp grid
	// Lauchpad shows and controls ranges:
	// [x ; x+8] [y ; y+8]
	upos_t currentPosition{ 0, 0 };

	FrameBase(const FrameBase& frame) = default;
	FrameBase(FrameBase&& frame) = default;
	~FrameBase() = default;

	auto getSize() const { return this->size; }

	auto getWidth() const { return this->size.width; }

	auto getHeight() const { return this->size.height; }

	auto getData() const { return this->data; }

	// Returns true if given point is in data's boundary
	// false otherwise
	bool isPointInBoundary(uint32_t x, uint32_t y) const;

	// which is X + currentPosition.x
	auto translateX(upos_t::underlying_type x) const
	{
		static_assert(std::is_same_v<decltype(x), decltype(this->currentPosition.x)>);
		return x + this->currentPosition.x;
	}

	// translates button pressed Y to offset(Y)
	// which is Y + currentPosition.y
	auto translateY(upos_t::underlying_type y) const
	{
		static_assert(std::is_same_v<decltype(y), decltype(this->currentPosition.y)>);
		return y + this->currentPosition.y;
	}

	// Sets value in maintable on position described as 3 dim space
	// Returns true if setting successed, false otherwise
	// 8 bit variant sets single byte value
	// 32 bit variant sets whole 3 byte color -- MSB is ignored
	bool dim3Set8(uint32_t x, uint32_t y, uint32_t z, byte value);
	bool dim3Set32(uint32_t x, uint32_t y, uint32_t value);

	// Fills given byte by reference with
	// value from maintable from position described as 3 dim space
	// Returns true if filling successed, false otherwise
	// 8 bit variant fils single byte value
	// 32 bit variant fils whole 3 byte color -- MSB is 0
	// CAUTION: in 32 bit variant value is filled backwards i.e.
	// like little endian should be to represent byte packed array well
	bool dim3Get8(uint32_t x, uint32_t y, uint32_t z, byte* value) const;
	bool dim3Get32(uint32_t x, uint32_t y, uint32_t* value) const;

	// Checks if given point by XY is in boundary and if so inserts it's
	// color given in third argument as packed uint32_t at given coordinates
	void insertColorIfInBoundary(uint32_t x, uint32_t y, uint32_t color);

	// Very similar to insertColorIfInBoundary but additionaly checks if
	// color is black, if so pixel is transparent and does not overwrite
	// pixel on that pos
	void insertTransparentableColorIfInBoundary(uint32_t x,
	                                            uint32_t y,
	                                            uint32_t color);

	// CAUTION: this does not insert one byte into memory! It inserts all 24
	// bits of color. 8 bit refers to span of each color. Mk2 opeartes on
	// colors in range 00-3f but piwo7 provides 00-ff colors. This method is
	// used to also convert value to be in mk2 span.
	void insertTransparentable8bitColorIfInBoundary(uint32_t x,
	                                                uint32_t y,
	                                                uint32_t color);

	uint32_t checkFixResolution(uint32_t& width, uint32_t& height);
	bool checkResolution(uint32_t width, uint32_t height);

	uint32_t getTotalFrameSize() const
	{
		return this->size.getTotal() * BYTES_PER_PIXEL;
	}

	byte& operator[](size_t index) const
	{
		return this->data[index];
	}

	friend inline int copyFrame(FrameBase& frame1, const FrameBase& frame2);
};

inline int
copyFrame(FrameBase& frame1, const FrameBase& frame2)
{
	if (frame1.size != frame2.size)
		return -1;

	auto f1begin = frame1.data;
	auto f2begin = frame2.data;
	auto f2end = f2begin + frame2.getTotalFrameSize();
	std::copy(f2begin, f2end, f1begin);

	return 0;
}

inline void
copyFrameVisible(FrameBase& frame1, const FrameBase& frame2)
{
	assert(frame1.getWidth() >= LP_GRID_WIDTH);
	assert(frame1.getHeight() >= LP_GRID_HEIGHT);

	for (uint32_t y = 0; y < LP_GRID_HEIGHT; ++y) {
		for (uint32_t x = 0; x < LP_GRID_WIDTH; ++x) {

			// If pixel out of bound, write 0
			uint32_t valueSrc = 0;
			uint32_t valueDst = 0;

			const auto xSrc = frame2.translateX(x);
			const auto ySrc = frame2.translateY(y);

			const auto xDst = frame1.translateX(x);
			const auto yDst = frame1.translateY(y);

			frame2.dim3Get32(xSrc, ySrc, &valueSrc);
			frame1.dim3Get32(xDst, yDst, &valueDst);

			if (valueDst == valueSrc)
				valueSrc = 0x00ffffff;

			frame1.dim3Set32(xDst, yDst, valueSrc);
		}
	}
}

class Frame : public FrameBase
{
public:
	Frame(uint32_t width, uint32_t height);
	Frame(const Frame& frame);
	Frame(Frame&& frame);
	~Frame();

	// Resizes current memory to the new size described by params
	// and resets current position to 0
	// If the size is the same memory is not reallocated and
	// position is not set to 0
	int resizeMemory(uint32_t width, uint32_t height);

private:
	void allocNewMemory(uint32_t width, uint32_t height);
	void deallocMemory();
};

// Creates default rjson document object for shared frames
static rapidjson::Document
defSFDoc()
{
	rapidjson::Document doc;
	doc.Parse("\
{\"display_width\":0,\"display_height\":0,\"row_size\":0,\
\"fps\":0,\"data_offset\":0,\"size\":0}");
	return doc;
}

class SharedFrame : public FrameBase
{
public:
	using shmem_t = mipc::shmem::shmem_t;
	using json_t = rapidjson::Document;
	using value_t = rapidjson::Value;
	using strbuffer_t = rapidjson::StringBuffer;
	using jsonwriter_t = rapidjson::Writer<strbuffer_t>;

private:
	const std::string_view name;
	shmem_t shmem;
	json_t doc = defSFDoc();

public:
	SharedFrame(std::string_view name, uint32_t width, uint32_t height)
	  : name(name)
	{
		createSharedFrame(name, width, height);
	}

	void createSharedFrame(std::string_view name,
	                       uint32_t width,
	                       uint32_t height)
	{
		debug_print("Allocating new shared frame [{}] [{}x{}]\n",
		            name,
		            width,
		            height);
		this->size.width = width;
		this->size.height = height;
		this->doc["display_width"].SetUint(width);
		this->doc["display_height"].SetUint(height);
		this->doc["row_size"].SetUint(width * BYTES_PER_PIXEL);
		this->doc["fps"].SetUint(static_cast<uint64_t>(FPSLOCK));
		this->doc["data_offset"].SetUint(0);
		this->doc["size"].SetUint(this->getTotalFrameSize());

		strbuffer_t strbuff;
		jsonwriter_t writer(strbuff);
		this->doc.Accept(writer);

		if (this->shmem.is_valid())
			this->shmem.close();

		this->shmem = shmem_t::create_(
		  name.data(), strbuff.GetString(), strbuff.GetSize());

		this->data = reinterpret_cast<byte*>(
		  this->shmem.is_valid() ? this->shmem.get_payload() : nullptr);
	}

	void resizeMemory(uint32_t nw, uint32_t nh)
	{
		createSharedFrame(this->name.data(), nw, nh);
	}

	bool shmem_valid() const { return this->shmem.is_valid(); }
	std::string_view shmem_name() const { return this->name; }
};

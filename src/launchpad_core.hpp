#pragma once

#include <stdint.h>
#include <stdio.h>
#include <tuple>

#include "mk2types.h"
#include "util/debugprint.h"

#include "IInputHandler.h"

#ifdef _WIN32
#include <windows.h>
#elif __linux__
#include "SuspendableThread.hpp"
#include <fcntl.h>
#include <thread>
#include <unistd.h>
#endif

class Mk2;

#ifdef _WIN32

inline constexpr WORD MK2_VID = 0x1235;
inline constexpr WORD MK2_PID = 0x0069;
inline constexpr std::string_view MK2_SZPNAME = "MK2";

extern void CALLBACK
MidiInputProc(HMIDIIN hMidiIn,
              uint32_t wMsg,
              DWORD_PTR dwInstance,
              DWORD_PTR dwParam1,
              DWORD_PTR dwParam2);

extern void CALLBACK
MidiOutputProc(HMIDIOUT hmo,
               uint32_t wMsg,
               DWORD_PTR dwInstance,
               DWORD_PTR dwParam1,
               DWORD_PTR dwParam2);

class Mk2Core
{
private:
	HMIDIIN hMidiDeviceIn;
	HMIDIOUT hMidiDeviceOut;
	mutable MIDIHDR midiOutPacket;

	// prints all devices in a simple list
	void printMidiDevices(const uint32_t devOutCnt,
	                      const uint32_t devInCnt) const;

	// prints midi devices choosed with labels
	// first argument is input device id
	// second is output device id
	void printMidiDevicesByID(const uint32_t indevid,
	                          const uint32_t outdevid) const;

protected:
	// handles return value of midiInOpen
	void handleMMSYSERR(const MMRESULT ret) const;

	void init(ButtonActionHandler* handler);

	// Closes communication with launchpad
	int close();

public:
	Mk2Core(ButtonActionHandler* handler)
	{
		this->init(handler);

		// Creates connection with launchpad hardware
		if (this->hMidiDeviceIn == 0 or this->hMidiDeviceOut == 0) {
			fprintf(stderr, "Connection to launchpad MK2 failed!\n");
			exit(1);
		}
	}

	~Mk2Core() = default;

	// Writes raw data to launchpad device
	ssize_t write(std::basic_string_view<byte> data) const;

	friend void CALLBACK ::MidiInputProc(HMIDIIN hMidiIn,
	                                     uint32_t wMsg,
	                                     DWORD_PTR dwInstance,
	                                     DWORD_PTR dwParam1,
	                                     DWORD_PTR dwParam2);

	friend void CALLBACK ::MidiOutputProc(HMIDIOUT hmo,
	                                      uint32_t wMsg,
	                                      DWORD_PTR dwInstance,
	                                      DWORD_PTR dwParam1,
	                                      DWORD_PTR dwParam2);
};
#elif __linux__
class Mk2Core;
extern void
MidiInputReader(Mk2Core& master);
class Mk2Core
{
private:
	int deviceDescriptorRead;
	int deviceDescriptorWrite;

	ButtonActionHandler* button_handler;

	void init(ButtonActionHandler* handler);
	volatile bool stopReadingThread = false;

protected:
	// Thread that reads from launchpad and calls
	// client callback with data sent by launchpad device.
	SuspendableThread readingThread;

public:
	Mk2Core(ButtonActionHandler* handler);

	~Mk2Core();

	// Writes raw data to launchpad device
	ssize_t write(std::basic_string_view<byte> data) const;

	friend void MidiInputReader(Mk2Core& master);
};
#endif

#ifndef _RENDEROBJECT_H_
#define _RENDEROBJECT_H_

#include "mk2Scene.h"
#include "util/debugprint.h"
#include "util/time.h"

#include <piwo/animation_view.h>
#include <piwo/frame_iterator.h>

#include <list>
#include <math.h>
#include <time.h>

typedef unsigned char byte;

// Class of objects that represent every animation spawned in RenderEngine.
// All the spawned objects will be hold in 'to render' array.
// The rendering engine will go through the table and if time has come
// render new frame from animation. If the time of whole animation ends
// ie there is no more frames to render it will unlink the animation objects
// and free its memory. All objects of the same type should share animation
// file which will be copied to RAM and accessed via pointer.
// If the color of animation is RGB = 000 then its 100% transparent
// and should not overprint animation pixel below.
// currentPosition inherited from mk2MainScene will be used as currentOffset
// of the RenderObject
class RenderObject
{
protected:
	// Holds XY offset of spawned animation
	// ie. where the user pushed the button on lp
	upos_t offset;

	// Holds time when new frame should be rendered
	time_point_def_t nextFrameTime;

	piwo::animation_view animation;
	piwo::frame_iterator_t current_frame;

	// Set to 1 if char* to animation reached eof
	// set to 0 otherwise
	bool animationEnded;

public:
	// Takes offset on main table of spawned object and pointer to its
	// animation
	explicit RenderObject(uint32_t xcoord,
	                      uint32_t ycoord,
	                      piwo::animation_view animptr);

	~RenderObject() = default;

	// Parsing next frame from animation pointer and flushes it to frame
	// passed as arg If the pixel's rgb value is 000 pixel will not be
	// flushed to prevent overpainting other animations with black pixels
	// Uses size of frame to determine size of data to read If end of
	// animation is reached function sets this->animationEnded to true
	// BEFORE CALL TO THIS FUNCTION POINTER SHOULD BE ON EMPTY LINE JUST
	// BEFORE WHERE THE DURATION OF FRAME IS. OTHERWISE ITS UNDEFINED
	// BEHAVIOR
	void flushFrame(FrameBase& frame);

	// Returns value of hasAnimationEnded member
	bool hasAnimationEnded();

	// Checks if its time or pass time to render an objects
	// Returns false when currentTime didnt reached yet nextFrameTime
	// True otherwise. Takes pointer to 64bit int as argument which will be
	// filled with current time. Agrument can be null, will be ignored then.
	bool isTimeToRender();

	// Used for debug nothing interesting rly
	uint32_t getObjectCount();
	uint32_t getObjectID();

	// Used for debuging information
	// Prints most of objects infomration
	void printMe();
};

// Iterates through list and returns iterator for indexed element
std::list<RenderObject*>::iterator
at(std::list<RenderObject*>& renderList, uint32_t index);

#endif // _RENDEROBJECT_H_

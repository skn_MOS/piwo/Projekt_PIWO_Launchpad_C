#include "mk2MainScene.h"

#include "RenderObject.h"
#include "launchpad.h"
#include "mk2PrintScene.h"
#include "util/utils.h"

#include <filesystem>
#include <functional>
#include <iostream>

void
readPipeThread(mk2MainScene* mainSceneptr);
void
requestsHandler(mk2MainScene* mainSceneptr);

struct resetPipeArgs
{
	mk2MainScene* mainSceneptr;
	char* pipeName;
	int32_t forced;
};

mk2MainScene::mk2MainScene(Mk2* lp, uint32_t w, uint32_t h)
  : mk2Scene(w, h, lp)
  , RenderEngineCore<mk2MainScene>(&this->mainTable,
                                   mk2MainScene::renderShmName)
  , masterOutPipeListener()
  , masterOutPipe()
  , pipeHandler(&readPipeThread, this)
  , currentAnim(invalidAnim)
  , currentColor(0)
  , animBinded(false)
{
	// Used when allocating animations
	std::array<char, 24> animationFilename_;
	auto animationFilename = animationFilename_.data();
	const char* filenameTemplate =
	  "." PATH_SEPARATOR "animations" PATH_SEPARATOR "8%i.piwo7";

	this->masterOutPipeListener =
	  mipc::pipe::listener_t::create("lpd_fifo");
	if (!this->masterOutPipeListener.is_valid())
		debug_printerr("Failed to create listener CTOR\n");
	this->pipeHandler.resume();

	// Offset to first X
	for (int32_t i = 0; i < ANIMATIONSCOUNT; i++) {
		sprintf(animationFilename, filenameTemplate, i);
		[[maybe_unused]] auto status =
		  this->animations[i].build_from_file(animationFilename);

#if !defined(LPD_WITH_NCURSES)
		if (status) {
			fprintf(
			  stderr,
			  "[MAINSCENE][ANIMATION ASIGNMENT] "
			  "Successfully loaded animation from file: %s -- "
			  "Addr: %p\n",
			  animationFilename,
			  this->animations[i].data());
		} else {
			fprintf(stderr,
			        "[MAINSCENE][ANIMATION ASIGNMENT] "
			        "Failed to load animation from file: %s\n",
			        animationFilename);
		}
#endif
	}

	this->getRenderCore().resumeRenderThread();

	auto clihandler_resize = [this](std::string_view args) -> CliResult {
		uint32_t nw, nh;

		if (sscanf(args.data(), "%u %u", &nw, &nh) != 2)
			return CliResultError(
			  "Bad argument format. Expected mainscene.resize "
			  "<new_width> <new_height>");

		if (this->reallocMainMem(nw, nh))
			return CliResultError(
			  "Bad argument. Width or height out of bounds.");

		return CliResultOk();
	};

	auto clianim_info =
	  [this]([[maybe_unused]] std::string_view args) -> CliResult {
		this->getRenderCore().printAllObjectsInfo();
		return CliResultOk();
	};

	auto clihandler_print = [this](std::string_view args) -> CliResult {
		if (args.size() == 0)
			return CliResultError(
			  "Expected string to print, got nothing");

		auto printScene = static_cast<mk2PrintScene*>(
		  this->parent->getScene(SCENEID::PRINTSCENEID));
		printScene->print(args);

		return CliResultOk();
	};

	this->cliiface::add(
	  CLIEntry{ "resize", "resizes main memory", clihandler_resize });
	this->cliiface::add(
	  CLIEntry{ "print", "prints string on the lp", clihandler_print });
	this->cliiface::add(
	  CLIEntry{ "infoobj", "prints render obj info", clianim_info });
}

void
readPipeThread(mk2MainScene* ms)
{
	// CAUTION THIS THREAD IS FORCEFULLY TERMINATED DURING MK2MAINSCENES
	// DESTURCTION HENCE IT SHOULD NOT MAKE ANY OBJECTS THAT REQUIRE RAII
	// CLEANUP OR IT WILL LEAK!

	constexpr size_t readCount = lg_ip_resize_total_size<size_t>;

	lg_local_buf_t buff;

	int read = ms->masterOutPipe.read(reinterpret_cast<char*>(buff.data()),
	                                  readCount);

	// After this line, all handlers except LG_IP_END handler can assume
	// that read > 0
	if (UNLIKELY(read <= 0)) {
		buff[0] = LG_IP_END;
	}

	debug_print("Last packet: {}\n", buff[0]);

	switch (buff[0]) {
		case LG_IP_NOP:
			debug_print("Got LG_IP_NOP");
			lg_ip_nop_handler(ms);
			break;

		case LG_IP_END:
			debug_print("Got LG_IP_END");
			lg_ip_end_handler(ms);
			break;

		case LG_IP_RESIZE:
			debug_print("Got LG_IP_RESIZE");
			lg_ip_resize_handler(
			  ms, buff, static_cast<uint32_t>(read));
			break;

		case LG_IP_ACCEPT:
			debug_print("Gor LG_IP_ACCEPT");
			lg_ip_accept_handler(ms);
			break;

		case LG_IP_REJECT:
			debug_print("Gor LG_IP_REJECT");
			lg_ip_reject_handler(ms);
			break;

		default:
			debug_print("Unexpected packet {}\n", buff[0]);
			break;
	}
}

mk2MainScene::~mk2MainScene()
{
	this->getRenderCore().killRenderThread();
	this->pipeHandler._terminate();
}

int32_t
mk2MainScene::initPipeForOutData(const char* pipename, int32_t forced)
{
	int32_t ret;

	puts(forced == 1 ? "Forced" : "Not forced");
	if (forced)
		ret = this->createPipeForOutDataForced(pipename);
	else
		ret = this->createPipeForOutData(pipename);

	// User said not to open
	if (!!ret)
		return ret;

	debug_print("RET = {}\n", ret);
	return ret;
}

int32_t
mk2MainScene::sendPipeDataWCheck(const void* data,
                                 const uint32_t datasize) const
{
	if (UNLIKELY(!this->masterOutPipe.is_valid()))
		return 0;

	return this->masterOutPipe.write(static_cast<const char*>(data),
	                                 datasize);
}

int32_t
mk2MainScene::createPipeForOutData(const char* pipename)
{
	char ch;
	if (this->masterOutPipe.is_valid()) {
		printf("Error: Cannot open pipe - already opened.\n"
		       "Want to reopen ? (y/N): ");
		ch = static_cast<char>(getchar());
		if (ch == 'N' || ch == 'n' || ch == '\n')
			return -2;
	}

	return this->createPipeForOutDataForced(pipename);
}

static mipc::pipe::conn_t openPipe(const char* pipename) noexcept
{
	auto listener = mipc::pipe::listener_t::create(pipename);
	return listener.accept();
}

int32_t
mk2MainScene::createPipeForOutDataForced(const char* pipename)
{
	auto newPipe = openPipe(pipename);

	if (!newPipe.is_valid()) {
		puts("Failed to open pipe.");
		return -2;
	}

	this->masterOutPipe = std::move(newPipe);

	if (this->masterOutPipe.is_valid()) {
		debug_print("Pipe valid");
	}

	this->pipeHandler.resume();
	return 0;
}

void
mk2MainScene::handleButtonPressed(const mk2Button& button)
{
	auto lp = this->parent;

	debug_print("hbp called. Got btn: {}\n", (uint32_t)button.XY);
	debug_print("Animbinded: {}. CurrentAnim: {}\n",
	            (uint32_t)this->animBinded,
	            (uint32_t)this->currentAnim);

	switch (button.XY) {
		// Switch to color picking
		case 0x87:
			this->parent->switchScene(SCENEID::COLORPICKSCENEID);
			break;
		case 0x79:
			log_stdout("GAME!\n");
			this->parent->switchScene(SCENEID::GAMEPICKSCENEID);
			break;
		// Clear whole pad
		case 0x80:
			lp->clearPad();
			this->restoreFunctionButtons();
			this->restorePositionState();
			this->restoreAnimState();
			this->zeroMemory();
			break;
		case 0x86:
			// eraser is active
			// so deactivate now
			if (this->currentColor & 0xff000000) {
				this->currentColor &= 0x00ffffff;
				lp->setLedXY(0x08, 0x06, 0x00000000);
			}

			// eraser is not active
			// so activate now
			else {
				this->currentColor |= 0xff000000;
				lp->setLedXY(0x08, 0x06, 0x003f3f3f);
			}
			break;
		// Animation
		case 0x81 ... 0x85:
			this->unselectAllAnims();
			this->selectAnim(button.XY - 0x81);
			break;
		// uparrow
		case 0x39:
			if (this->mainTable.currentPosition.y + 8 <
			    this->mainTable.getHeight()) {
				this->mainTable.currentPosition.y++;
				this->restorePositionState();
				this->restoreGrid();
			}
			break;
		// downarrow
		case 0x49:
			if (this->mainTable.currentPosition.y > 0) {
				this->mainTable.currentPosition.y--;
				this->restorePositionState();
				this->restoreGrid();
			}
			break;
		// leftarrow
		case 0x59:
			if (this->mainTable.currentPosition.x > 0) {
				this->mainTable.currentPosition.x--;
				this->restorePositionState();
				this->restoreGrid();
			}
			break;
		// rightarrow
		case 0x69:
			if (this->mainTable.currentPosition.x + 8 <
			    this->mainTable.getWidth()) {
				this->mainTable.currentPosition.x++;
				this->restorePositionState();
				this->restoreGrid();
			}
			break;
		//	painting
		default:
			uint32_t x = (button.XY & 0xf0) >> 4;
			uint32_t y = button.XY & 0x0f;
			uint32_t color = this->currentColor;
			// eraser
			if (this->currentColor & 0xff000000) {
				color = 0;
				if (lp->setGridLedXY(x, y, 0)) {
					x = this->mainTable.translateX(x);
					y = this->mainTable.translateY(y);
					this->mainTable.dim3Set32(x, y, 0);
				}
			}
			// animation
			else if (this->animBinded) {
				auto posx =
				  x + this->mainTable.currentPosition.x;
				auto posy =
				  y + this->mainTable.currentPosition.y;
				// Asigns animation acording to button pressed
				std::optional robj_opt =
				  this->createRenderObject(
				    posx, posy, this->currentAnim);

				if (LIKELY(robj_opt.has_value()))
					this->getRenderCore().spawnNewObject(
					  robj_opt.value());
			}
			// default painting
			else {
				if (lp->setGridLedXY(
				      x, y, this->currentColor)) {
					x = this->mainTable.translateX(x);
					y = this->mainTable.translateY(y);
					this->mainTable.dim3Set32(x, y, color);
				}
			}
			break;
	}
	return;
}

void
mk2MainScene::zeroMemory()
{
	for (uint32_t i = 0; i < this->mainTable.getHeight(); i++) {
		for (uint32_t j = 0; j < this->mainTable.getWidth(); j++) {
			this->mainTable.dim3Set32(j, i, 0);
		}
	}

	return;
}

void
mk2MainScene::showTable()
{
	uint32_t color;

	for (auto i = this->mainTable.getHeight(); i > 0; i--) {
		printf("[");
		for (uint32_t j = 0; j < this->mainTable.getWidth(); j++) {
			// Dont need to check return value
			// Upper checks in for loop are enough
			this->mainTable.dim3Get32(j, i - 1, &color);

			printf("[%02x %02x %02x],",
			       static_cast<byte>(color >> 16),
			       static_cast<byte>(color >> 8),
			       static_cast<byte>(color));
		}
		printf("]\n");
	}
	printf("CURRENT FRAME XY: %x %x\n",
	       this->mainTable.currentPosition.x,
	       this->mainTable.currentPosition.y);
}

void
mk2MainScene::restoreFunctionButtons()
{
	this->parent->setLedXY(8, 7, this->currentColor);
	this->parent->pulseLedXY(7, 9, Mk2Color::purple);
	this->parent->setLedXY(8, 0, 0);

	// eraser
	if (this->currentColor & 0xff000000)
		this->parent->setLedXY(8, 6, 0x003f3f3f);
	else
		this->parent->setLedXY(8, 6, 0x00000000);

	this->restorePositionState();
}

void
mk2MainScene::restoreGrid()
{
	this->flushMainTableToLaunchpad();
	this->restoreFunctionButtons();
	this->restoreAnimState();

	return;
}

void
mk2MainScene::restoreAnimState()
{
	this->unselectAllAnimsG();
	if (this->currentAnim < ANIMATIONSCOUNT)
		this->parent->setLedXY(8, this->currentAnim + 1, 0x3f3f3f);
}

void
mk2MainScene::enter()
{
	this->parent->clearPad();
	this->restoreFunctionButtons();
	this->restorePositionState();
	this->restoreGrid();
	if (this->resumeRenderOnRestore)
		this->getRenderCore().resumeRenderThread();
}

void
mk2MainScene::leave()
{
	this->resumeRenderOnRestore = !this->getRenderCore().isStopRequested();
	this->getRenderCore().suspendAndWaitRenderThread();
}

int
mk2MainScene::reallocMainMem(uint32_t width, uint32_t height)
{
	if (!this->mainTable.checkResolution(width, height))
		return -1;

	bool isRenderingcopy = this->getRenderCore().isRendering();

	this->getRenderCore().suspendAndWaitRenderThread();

	this->mainTable.currentPosition.x = 0;
	this->mainTable.currentPosition.y = 0;

	this->mainTable.resizeMemory(width, height);

	this->restorePositionState();
	this->getRenderCore().updateCanvasBackgroundInfo(&this->mainTable);

	if (!this->canvas.shmem_valid()) {
		debug_print("Reallocation of shared frame failed!\n");
		isRenderingcopy = false;
	}

	if (isRenderingcopy)
		this->getRenderCore().resumeRenderThread();

	return 0;
}

std::optional<RenderObject>
mk2MainScene::createRenderObject(uint32_t xoffset,
                                 uint32_t yoffset,
                                 uint32_t animID)
{
	// Animation of out bound
	if (animID > ANIMATIONSCOUNT) {
		debug_print("Out of bound animation {}\n", animID);
		return std::nullopt;
	}

	auto& selected_animation = this->animations[animID];
	return RenderObject(xoffset, yoffset, selected_animation);
}

void
mk2MainScene::unselectAllAnims()
{
	this->animBinded = false;
	this->unselectAllAnimsG();
}

void
mk2MainScene::unselectAllAnimsG()
{
	for (int32_t i = ANIMATIONSCOUNT - 1; i >= 0; i--) {
		if (this->animations[i].frame_count() == 0)
			// If not loaded set red
			this->parent->setLedXY(
			  8, static_cast<uint32_t>(i + 1), 0x003f0000);
		else
			// If loaded set green
			this->parent->setLedXY(
			  8, static_cast<uint32_t>(i + 1), 0x00003f00);
	}
}

bool
mk2MainScene::handleGUIResize(uint32_t nw, uint32_t nh)
{
	uint32_t packet[] = { LG_OP_ACCEPT, LG_IP_RESIZE, LG_OP_SHMRELOAD };

	if (this->reallocMainMem(nw, nh)) {
		packet[0] = LG_OP_REJECT;
		sendPipeDataWCheck(&packet, sizeof(uint32_t) * 2);
		return false;
	}

	sendPipeDataWCheck(&packet, sizeof(uint32_t) * 3);
	return true;
}

bool
mk2MainScene::selectAnim(uint32_t animation)
{
	// Animation out of table
	if (animation > ANIMATIONSCOUNT) {
		return false;
	}

	this->unselectAllAnimsG();

	if (this->currentAnim == animation) {
		this->currentAnim = invalidAnim;
		this->animBinded = false;
		return true;
	}

	if (this->animations[animation].frame_count() != 0) {
		this->currentAnim = animation;
		this->animBinded = true;
		this->parent->setLedXY(8, animation + 1, 0x003f3f3f);
		return true;
	}

	return false;
}

void
mk2MainScene::closePipe()
{
	this->masterOutPipe.close();
}

void
mk2MainScene::renderCallback(FrameBase* frame) const
{
	frame->currentPosition = this->mainTable.currentPosition;
	this->parent->flushFrameToLaunchpad(*frame);
}

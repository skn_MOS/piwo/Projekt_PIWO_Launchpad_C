#include "mk2PrintScene.h"
#include "launchpad.h"

void
mk2PrintScene::handleButtonPressed(const mk2Button& button)
{
	auto lp = this->parent;

	debug_print("Got button: {}x{} on board: {}x{}\n",
	            static_cast<uint32_t>(button.XY >> 4),
	            static_cast<uint32_t>(button.XY & 0x0f),
	            this->mainTable.getWidth(),
	            this->mainTable.getHeight())

	  switch (button.XY)
	{
		case 0x00:
			lp->switchScene(SCENEID::PRINTSCENEID);
			break;
		default:
			break;
	}

	return;
}

void
mk2PrintScene::restoreScene()
{
	this->clearPad();
}

void
mk2PrintScene::enter()
{
#if defined(__WIN32)
	log_stderr("WARNING: Due to bug in Windows driver for MK2 this scene "
	           "can misbehave\n");
#endif

	auto arg =
	  reinterpret_cast<std::string_view*>(this->parent->argStack.pop());
	assert(arg);
	this->parent->setString(*arg, Mk2Color::yellow, Mk2Speed::slow);
}

void
mk2PrintScene::leave()
{}

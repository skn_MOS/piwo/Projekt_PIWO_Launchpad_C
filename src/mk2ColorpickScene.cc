#include "mk2ColorpickScene.h"
#include "launchpad.h"
#include <stdio.h>

mk2ColorpickScene::mk2ColorpickScene(uint32_t w, uint32_t h, Mk2* parent)
  : mk2Scene(w, h, parent)
{
	this->generatePalette();
}

void
mk2ColorpickScene::handleButtonPressed(const mk2Button& button)
{
	auto lp = this->parent;
	uint32_t color;

	debug_print("Got button: {}x{} on board: {}x{}\n",
	            static_cast<uint32_t>(button.XY >> 4),
	            static_cast<uint32_t>(button.XY & 0x0f),
	            this->mainTable.getWidth(),
	            this->mainTable.getHeight())

	  switch (button.XY)
	{
		case 0x87:
			lp->switchScene(SCENEID::COLORPICKSCENEID);
			break;
		// uparrow
		case 0x39:
			if (this->mainTable.currentPosition.y + 8 <
			    this->mainTable.getHeight()) {
				this->mainTable.currentPosition.y++;
				this->restorePositionState();
				this->restoreGrid();
			}
			break;
		// downarrow
		case 0x49:
			if (this->mainTable.currentPosition.y > 0) {
				this->mainTable.currentPosition.y--;
				this->restorePositionState();
				this->restoreGrid();
			}
			break;
		// leftarrow
		case 0x59:
			if (this->mainTable.currentPosition.x > 0) {
				this->mainTable.currentPosition.x--;
				this->restorePositionState();
				this->restoreGrid();
			}
			break;
		// rightarrow
		case 0x69:
			if (this->mainTable.currentPosition.x + 8 <
			    this->mainTable.getWidth()) {
				this->mainTable.currentPosition.x++;
				this->restorePositionState();
				this->restoreGrid();
			}
			break;
		case 0x80:
			this->mainTable.currentPosition.x = 0;
			this->mainTable.currentPosition.y = 0;
			this->flushMainTableToLaunchpad();
			break;
		default:
			if (!utils::isPointInMainGrid(button.XY >> 4,
			                              button.XY & 0x0f))
				break;
			this->mainTable.dim3Get32(
			  this->mainTable.translateX(button.XY >> 4),
			  this->mainTable.translateY(button.XY & 0x0f),
			  &color);

			auto ms = lp->getScene(SCENEID::MAINSCENEID);
			this->changeMainsCurrentColor(color, ms);
			debug_print("Got color: {}\n", color);
			lp->switchScene(SCENEID::COLORPICKSCENEID);
	}
	return;
}

void
mk2ColorpickScene::changeMainsCurrentColor(uint32_t color, mk2MainScene* ms)
{
	ms->currentColor = color;
}

void
mk2ColorpickScene::changeMainsCurrentColor(uint32_t color, mk2Scene* ms)
{
	auto mms = static_cast<mk2MainScene*>(ms);
	mms->currentColor = color;
}

void
mk2ColorpickScene::makeMiddleChessy()
{
	// =====
	// Makes colorpick looks like chess board in the middle
	// to make picking white and black color
	// |X|O|
	// |O|X|
	// =====

	// Setting right top middle to white
	this->mainTable.dim3Set32(4, 4, 0x3f3f3f);

	// Setting left bottom middle to white
	this->mainTable.dim3Set32(3, 3, 0x3f3f3f);

	// Setting left top middle to black
	this->mainTable.dim3Set32(3, 4, 0x000000);

	// Setting right bottom middle to black
	this->mainTable.dim3Set32(4, 3, 0x000000);
}

void
mk2ColorpickScene::restoreScene()
{
	this->parent->clearPad();
	this->restoreGrid();
	this->restorePositionState();
	this->parent->setLedXY(0x08, 0x00, 0x003f3f3f);
}

void
mk2ColorpickScene::enter()
{
	this->restoreScene();
}

void
mk2ColorpickScene::leave()
{}

// TODO(holz) This implementation might be more compact
void
mk2ColorpickScene::generatePalette()
{
	constexpr uint32_t maxColor = 63;
	const uint32_t size =
	  this->mainTable.getWidth() * this->mainTable.getHeight();
	const uint32_t maxLv =
	  static_cast<uint32_t>((static_cast<float>(size) / 5));
	const float maxLv_float = static_cast<float>(maxLv);

	// Multipliers of color value
	uint32_t red = maxLv;
	uint32_t grn = 1;
	uint32_t blu = 1;

	// Holds current offset in mainTable
	uint32_t curWidth = 0;
	uint32_t curHeight = 0;

	for (uint32_t i = 0; i < maxLv; i++) {
		if (!this->wrapWidthToBoundary(&curWidth, &curHeight))
			return;

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  0,
		  static_cast<byte>(static_cast<float>(red * maxColor) /
		                    maxLv_float));

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  1,
		  static_cast<byte>(static_cast<float>(grn * maxColor) /
		                    maxLv_float));

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  2,
		  static_cast<byte>(static_cast<float>(blu * maxColor) /
		                    maxLv_float));

		debug_print("[{}]:[{}] {} {} {}\n",
		            curWidth,
		            curHeight,
		            static_cast<float>(red * maxColor) / maxLv_float,
		            static_cast<float>(grn * maxColor) / maxLv_float,
		            static_cast<float>(blu * maxColor) / maxLv_float);

		curWidth++;
		grn += 1;

		if (grn > maxLv)
			grn = maxLv;
	}

	for (uint32_t i = 0; i < maxLv; i++) {
		if (!this->wrapWidthToBoundary(&curWidth, &curHeight))
			return;

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  0,
		  static_cast<byte>(static_cast<float>(red * maxColor) /
		                    maxLv_float));

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  1,
		  static_cast<byte>(static_cast<float>(grn * maxColor) /
		                    maxLv_float));

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  2,
		  static_cast<byte>(static_cast<float>(blu * maxColor) /
		                    maxLv_float));

		debug_print("[{}]:[{}] {} {} {}\n",
		            curWidth,
		            curHeight,
		            static_cast<float>(red * maxColor) / maxLv_float,
		            static_cast<float>(grn * maxColor) / maxLv_float,
		            static_cast<float>(blu * maxColor) / maxLv_float);

		curWidth++;
		red -= 1;

		if (grn > maxLv)
			grn = maxLv;
	}

	red = 0;

	for (uint32_t i = 0; i < maxLv; i++) {
		if (!this->wrapWidthToBoundary(&curWidth, &curHeight))
			return;

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  0,
		  static_cast<byte>(static_cast<float>(red * maxColor) /
		                    maxLv_float));

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  1,
		  static_cast<byte>(static_cast<float>(grn * maxColor) /
		                    maxLv_float));

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  2,
		  static_cast<byte>(static_cast<float>(blu * maxColor) /
		                    maxLv_float));

		debug_print("[{}]:[{}] {} {} {}\n",
		            curWidth,
		            curHeight,
		            static_cast<float>(red * maxColor) / maxLv_float,
		            static_cast<float>(grn * maxColor) / maxLv_float,
		            static_cast<float>(blu * maxColor) / maxLv_float);

		curWidth++;
		blu += 1;

		if (blu > maxLv)
			blu = maxLv;
	}
	for (uint32_t i = 0; i < maxLv; i++) {
		if (!this->wrapWidthToBoundary(&curWidth, &curHeight))
			return;

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  0,
		  static_cast<byte>(static_cast<float>(red * maxColor) /
		                    maxLv_float));

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  1,
		  static_cast<byte>(static_cast<float>(grn * maxColor) /
		                    maxLv_float));

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  2,
		  static_cast<byte>(static_cast<float>(blu * maxColor) /
		                    maxLv_float));

		debug_print("[{}]:[{}] {} {} {}\n",
		            curWidth,
		            curHeight,
		            static_cast<float>(red * maxColor) / maxLv_float,
		            static_cast<float>(grn * maxColor) / maxLv_float,
		            static_cast<float>(blu * maxColor) / maxLv_float);

		curWidth++;
		grn -= 1;
	}

	grn = 0;
	red = 1;

	for (uint32_t i = 0; i < maxLv; i++) {
		if (!this->wrapWidthToBoundary(&curWidth, &curHeight))
			return;

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  0,
		  static_cast<byte>(static_cast<float>(red * maxColor) /
		                    maxLv_float));

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  1,
		  static_cast<byte>(static_cast<float>(grn * maxColor) /
		                    maxLv_float));

		this->mainTable.dim3Set8(
		  curWidth,
		  curHeight,
		  2,
		  static_cast<byte>(static_cast<float>(blu * maxColor) /
		                    maxLv_float));

		debug_print("[{}]:[{}] {} {} {}\n",
		            curWidth,
		            curHeight,
		            static_cast<float>(red * maxColor) / maxLv_float,
		            static_cast<float>(grn * maxColor) / maxLv_float,
		            static_cast<float>(blu * maxColor) / maxLv_float);

		curWidth++;
		red += 1;

		if (red > maxLv)
			red = maxLv;
	}

	red = 0;
	blu = 0;

	if (!this->wrapWidthToBoundary(&curWidth, &curHeight))
		return;

	this->mainTable.dim3Set8(
	  curWidth,
	  curHeight,
	  0,
	  static_cast<byte>(static_cast<float>(red * maxColor) / maxLv_float));

	this->mainTable.dim3Set8(
	  curWidth,
	  curHeight,
	  1,
	  static_cast<byte>(static_cast<float>(grn * maxColor) / maxLv_float));

	this->mainTable.dim3Set8(
	  curWidth,
	  curHeight,
	  2,
	  static_cast<byte>(static_cast<float>(blu * maxColor) / maxLv_float));

	debug_print("[{}]:[{}] {} {} {}\n",
	            curWidth,
	            curHeight,
	            static_cast<float>(red * maxColor) / maxLv_float,
	            static_cast<float>(grn * maxColor) / maxLv_float,
	            static_cast<float>(blu * maxColor) / maxLv_float);

	makeMiddleChessy();
}

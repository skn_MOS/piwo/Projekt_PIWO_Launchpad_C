#!/bin/bash
find src -type f -regex ".*\.\(cpp\|cc\|hpp\|h\)" | xargs clang-format -i -style=file

#!/usr/bin/python3

import argparse
import subprocess

argparser = argparse.ArgumentParser()
argparser.add_argument("--remove", help="Remove udev rule", action="store_true")

args = argparser.parse_args()

filepath = "/etc/udev/rules.d/10-mos-piwo.rules"
rule = 'ACTION=="add", KERNEL=="midi*", ATTRS{idVendor}=="1235", ATTRS{idProduct}=="0069", MODE="0666", SYMLINK+="mk2launchpad"'

def append_rule():
    try:
        with open(filepath) as f:
            for line in f:
                if line == rule:
                    print("Already registered")
                    exit(0)
    except FileNotFoundError:
        pass

    with open(filepath, 'a+') as f:
        f.write(rule)


try: 
    if args.remove:
        import os
        os.remove(filepath)
    else:
        append_rule()
except PermissionError:
    print("Permission denied when writing to file. (Running this as root might fix it)")
    exit(1)

subprocess.call(['udevadm', 'control', '--reload'])
subprocess.call(['udevadm', 'trigger'])

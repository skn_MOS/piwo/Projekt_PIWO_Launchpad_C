import sys
import os
import glob
from shutil import copyfile

def expandargv():
    eargv = []
    for pattern in sys.argv[1:]:
        eargv += [x for x in glob.glob(pattern)]
    return eargv

bindedCount = 0
eargv = expandargv()
for filename in eargv:
    try:
        file = open(filename, 'rb')
    except IOError:
        print "Error: Cannot open {}".format(filename)
        continue

    signature = file.read().split('\n')[0]
    if signature.strip() == "PIWO_7_FILE":
        if bindedCount == 5:
            print "Warning: Too many animations. {} has correct signature but is ignored.".format(filename)
            continue
    
        bindedCount += 1
        
        if not os.path.exists("./animations/"):
            os.makedirs("./animations/")
       
        copyfile(filename, "./animations/8"+str(bindedCount)+".piwo7")

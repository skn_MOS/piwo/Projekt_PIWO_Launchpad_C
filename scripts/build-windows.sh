#!/bin/sh
mkdir lp-windows-build
cd lp-windows-build
cmake -GNinja -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../cmake/cross-compile/linux_cc_windows.cmake ..
ninja
